using UnityEngine;

using Mirror;

namespace BombGame
{
    public class GameControl : MonoBehaviour
    {
        private static readonly Quaternion Identity = Quaternion.identity;
        private static readonly Vector3 Zero = Vector3.zero;

        [SerializeField] private ElemTheme elemTheme = null;
        public ElemTheme ElemTheme => elemTheme;
        [SerializeField] private LevelControl level = null;

        public LevelControl Level => level;
        [SerializeField] private CharacterCameraControl characterCamera = null;
        public CharacterCameraControl CharacterCamera => characterCamera;

        [SerializeField] private UIControl ui = null;

        public UIControl UI => ui;

        private Transform cacheRoot;
        private ElemCache<ElemTheme.Type, CachedElem> elemCache;

        void Awake()
        {
            cacheRoot = new GameObject("cache_root").transform;
            cacheRoot.SetParent(transform);

            elemCache = new ElemCache<ElemTheme.Type, CachedElem>(cacheRoot, elemTheme);
        }

        private void Start()
        {
            elemCache.Cache(ElemTheme.Type.NormalBomb, 20);
        }

        public void AddCameraToPlayer(AvatarCharacterController player)
        {
            characterCamera.Character = player;
        }

        public GameObject SpawnAvatar(Vector3 position)
        {
            var playerPrefab = elemTheme.GetPrefab(ElemTheme.Type.PlayerAvatar);
            GameObject go = Instantiate(playerPrefab, position, Quaternion.identity);

            var avatar = go.GetComponent<AvatarCharacterController>();
            UI.AddCharacter(avatar);
            avatar.Spawned(this, go.transform.position);

            return go;
        }

        public void RespawnAvatar(Vector3 position, AvatarCharacterController avatar)
        {
            avatar.transform.position = position;
            avatar.Spawned(this, position);
        }

        public void UnspawnAvatar(GameObject go)
        {
            var avatar = go.GetComponent<AvatarCharacterController>();
            if (avatar)
            {
                UI.RemoveCharacter(avatar);
            }

            DestroyImmediate(go);
        }

        public GameObject SpawnObjectLocal(ElemTheme.Type type, Vector3 position)
        {
            var prefab = elemTheme.GetPrefab(type);
            var go = Instantiate(prefab, position, Identity);

            var cached = go.GetComponent<CachedElem>();
            cached.Game = this;

            return go;
        }

        public GameObject SpawnObject(ElemTheme.Type type, Vector3 position)
        {
            var go = SpawnObjectLocal(type, position);
            
            NetworkServer.Spawn(go);

            return go;
        }

        public GameObject SpawnObject(ElemTheme.Type type, Vector3 position, Vector3 force)
        {
            var go = SpawnObjectLocal(type, position);

            var body = go.GetComponent<Rigidbody>();
            if (body != null)
            {
                body.AddForce(force, ForceMode.Impulse);
            }

            NetworkServer.Spawn(go);

            return go;
        }

        public void UnspawnObject(GameObject go)
        {
            DestroyImmediate(go);
        }
    }
}