using UnityEngine;

namespace BombGame
{
    public class ExplosionTarget : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Events.UnityEvent<Vector3, ExplosionInfo> OnExplosion = null;

        public void Exploded(Vector3 center, ExplosionInfo explosionInfo)
        {
            OnExplosion?.Invoke(center, explosionInfo);
        }
    }
}