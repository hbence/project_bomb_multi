using UnityEngine;
using Mirror;

namespace BombGame
{
    public class TimerControl : MonoBehaviour
    {
        [SerializeField] private float duration = 0;
        [SerializeField] private UnityEngine.Events.UnityEvent onFinished = null;
        public float Duration { get => duration; set => duration = value; }

        public bool IsRunning { get; private set; } = false;

        public float ElapsedTime { get; private set; }
        public float ElapsedRatio => ElapsedTime / Duration;

        private void OnEnable()
        {
            Reset();
            Play();
        }

        void Update()
        {
            if (IsRunning)
            {
                ElapsedTime += Time.deltaTime;
                if (ElapsedTime > Duration)
                {
                    ElapsedTime = Duration;
                    onFinished?.Invoke();
                    Pause();
                }
            }
        }

        public void Reset()
            => Stop();

        public void Play()
        {
            IsRunning = true;
        }

        public void Pause()
        {
            IsRunning = false;
        }

        public void Stop()
        {
            IsRunning = false;
            ElapsedTime = 0;
        }

    }
}