using UnityEngine;

using Mirror;

namespace BombGame
{
    public class GrabController : MonoBehaviour
    {
        private static readonly string[] DefGrabLayers = { "Bomb" };
     
        [SerializeField] private Transform grabTransform = null;
        [SerializeField] private string[] grabLayers = DefGrabLayers;
        [SerializeField] private float grabRadius = 1f;
        public Transform GrabTansform => grabTransform;
        public Vector3 GrabbedPosition => grabTransform.position;
        public Vector3 GrabbedForward => grabTransform.forward;
        public Vector3 GrabbedUp => grabTransform.up;

        private int grabMask;
        private Collider[] grabBuffer;
        public GrabTarget GrabbedObject { get; private set; }
        public bool IsGrabbing => GrabbedObject != null;

        void Awake()
        {
            grabBuffer = new Collider[4];
            grabMask = Utils.CalcLayerMask(grabLayers);
        }

        public void Clear()
        {
            GrabbedObject = null;
        }

        void LateUpdate()
        {
            if (IsGrabbing)
            {
                GrabbedObject.transform.position = grabTransform.position;
            }
        }

        public void Grab(GrabTarget obj)
        {
            Drop();

            obj.Grab(grabTransform, OnObjectDropped);
            GrabbedObject = obj;
        }

        public void Drop()
        {
            if (IsGrabbing)
            {
                var objectToDrop = GrabbedObject;
                Clear();
                objectToDrop.Drop();
            }
        }

        private void OnObjectDropped(GrabTarget target)
        {
            // unintentional drop, for no just clear
            if (GrabbedObject == target)
            {
                Clear();
            }
        }

        public GrabTarget FindGrabTarget()
        {
            var hitNum = Physics.OverlapSphereNonAlloc(grabTransform.position, grabRadius, grabBuffer, grabMask);
            if (hitNum > 0)
            {
                int selected = 0;
                float minDist = Vector3.Distance(GrabbedPosition, grabBuffer[0].transform.position);
                for (int i = 1; i < hitNum; ++i)
                {
                    float dist = Vector3.Distance(GrabbedPosition, grabBuffer[i].transform.position);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        selected = i;
                    }
                }
                return grabBuffer[selected].GetComponent<GrabTarget>();
            }

            return null;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(grabTransform.position, grabRadius);
        }
    }
}
