using UnityEngine;

namespace BombGame
{
    public class ExplosionControl : MonoBehaviour
    {
        [SerializeField] private float explosionDuration = 1f;
        [SerializeField] private CachedElem cached = null;
        [SerializeField] private float cameraShakeDistance = 5f;
        [SerializeField] private float cameraShakeStress = 0.35f;

        private float timer = 0;
        public Vector3 Position { get; set; }

        private bool shook = false;

        private void OnEnable()
        {
            timer = 0;
            shook = false;
        }

        private void ShakeCamera()
        {
            if (cached.Game != null)
            {
                var camera = cached.Game.CharacterCamera;
                if (camera.Character != null)
                {
                    if (Vector3.Distance(camera.Character.transform.position, transform.position) < cameraShakeDistance)
                    {
                        camera.ShakableTransform.InduceStress(cameraShakeStress);
                    }
                }
            }
        }

        public void Init(ExplosionInfo info)
        {

        }

        private void Update()
        {
            if (!shook)
            {
                shook = true;
                ShakeCamera();
            }

            if (timer < explosionDuration)
            {
                timer += Time.deltaTime;
                if (timer >= explosionDuration)
                {
                    OnFinished();
                }
            }
        }

        public void OnFinished()
        {
            cached.Game.UnspawnObject(gameObject);
        }
    }
}
