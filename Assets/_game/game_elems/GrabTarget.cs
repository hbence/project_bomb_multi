using UnityEngine;

namespace BombGame
{
    public class GrabTarget : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Events.UnityEvent<Transform> onPickedUp = null;
        public UnityEngine.Events.UnityEvent<Transform> OnPickedUp => onPickedUp;
        [SerializeField] private UnityEngine.Events.UnityEvent onDropped = null;
        public UnityEngine.Events.UnityEvent OnDropped => onDropped;

        public Transform HoldingAnchor { get; private set; }
        public bool IsGrabbed => HoldingAnchor != null;

        private System.Action<GrabTarget> DropCallback { get; set; }

        public void Grab(Transform holdingAnchor, System.Action<GrabTarget> dropCallback = null)
        {
            Drop();

            HoldingAnchor = holdingAnchor;
            DropCallback = dropCallback;
            OnPickedUp?.Invoke(HoldingAnchor);
        }

        public void Drop()
        {
            if (IsGrabbed)
            {
                DropCallback?.Invoke(this);
                OnDropped?.Invoke();

                HoldingAnchor = null;
                DropCallback = null;
            }
        }

        private void OnDestroy()
        {
            Drop();
        }

        private void OnDisable()
        {
            Drop();
        }
    }
}
