using UnityEngine;

namespace BombGame
{
    [System.Serializable]
    public class ExplosionInfo
    {
        private static readonly Vector3 Up = Vector3.up;

        [SerializeField] private float radius;
        public float Radius { get => radius; private set => radius = value; }
        [SerializeField] private float force;
        public float Force { get => force; private set => force = value; }
        [SerializeField] private float uplift;
        public float Uplift { get => uplift; private set => uplift = value; }

        public ExplosionInfo(float radius, float force, float uplift)
        {
            Radius = radius;
            Force = force;
            Uplift = uplift;
        }

        public float CalcPushForce(float distance)
            => CalcPushForceConstant(distance);

        public Vector3 CalcPushForceVector(Vector3 bodyPos, Vector3 explosionCenter)
        {
            Vector3 delta = bodyPos - explosionCenter;
            delta.Normalize();
            delta *= CalcPushForce(delta.magnitude);

            delta += Up * uplift;

            return delta;
        }

        private float CalcPushForceConstant(float distance)
            => Force;

        private float CalcPushForceLinear(float distance)
            => Mathf.Lerp(force, 0, Mathf.Clamp01(distance / radius));

        private float CalcPushForceQuadratic(float distance)
            => Mathf.Lerp(force, 0, Mathf.Pow(Mathf.Clamp01(distance / radius), 2));

        public void AddPushForce(Vector3 explosionCenter, Rigidbody body)
        {
            Vector3 force = body.position - explosionCenter;
            float dist = force.magnitude;
            if (dist > 0)
            {
                force *= CalcPushForce(dist) / dist;
                body.AddForce(force, ForceMode.Impulse);
            }
        }
    }
}
