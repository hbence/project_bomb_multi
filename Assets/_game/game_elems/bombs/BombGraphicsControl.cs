using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BombGame
{
    public class BombGraphicsControl : MonoBehaviour
    {
        private const float DeltaEpsilon = 0.01f;

        [SerializeField] private TimerControl timer = null;
        [SerializeField] private Transform baseElem = null;
        [Header("materials")]
        [SerializeField] private MeshRenderer bombRenderer = null;
        [SerializeField] private Material defMaterial = null;
        [SerializeField] private Material redMaterial = null;
        [SerializeField] private float changeLimit = 0.03f;
        [Header("scaling")]
        [SerializeField] private AnimationCurve baseScaling = null;
        [SerializeField] private float scalingMultiplier = 1f;
        [Header("fuse")]
        [SerializeField] private Transform fuseElem = null;
        [SerializeField] private Vector3 fuseStart;
        [SerializeField] private Vector3 fuseEnd;

        private float originalScale = 1;

        private void Awake()
        {
            originalScale = baseElem.localScale.x;
        }

        private void OnEnable()
        {
            bombRenderer.sharedMaterial = defMaterial;
        }

        public void OnDropped()
        {
            transform.up = Quaternion.AngleAxis(30, transform.right) * transform.up;
        }

        void Update()
        {
            float ratio = timer.ElapsedRatio;
            HandleBaseScaling(ratio);
            PlaceFuse(ratio);
        }

        private void HandleBaseScaling(float ratio)
        {
            float vcScale = 1 + (baseScaling.Evaluate(ratio) - 1) * scalingMultiplier;
            float hzScale = 1f / vcScale;

            float delta = vcScale - 1f;

            vcScale *= originalScale;
            hzScale *= originalScale;
            baseElem.localScale = new Vector3(hzScale, vcScale, hzScale);

            bombRenderer.sharedMaterial = (Mathf.Abs(delta) > changeLimit) ? redMaterial : defMaterial;
        }

        private void PlaceFuse(float ratio)
        {
            fuseElem.localPosition = Vector3.Lerp(fuseStart, fuseEnd, ratio);
        }
    }
}
