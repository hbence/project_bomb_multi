using UnityEngine;

namespace BombGame
{
    public class BombRadiusSprite : MonoBehaviour
    {
        static private Vector3 Zero = Vector3.zero;
        static private Vector3 Down = Vector3.down;
        static private string GroundLayerName = "Ground";
        static private int GroundMask = -1;
        private const float HitTestHeight = 10f;
        private const float HitTestLength = 20f;

        private static readonly Vector3 Up = Vector3.up;

        [SerializeField] private SpriteRenderer sprite = null;
        [SerializeField] private TimerControl timer = null;
        [SerializeField] private float distanceFromBottom = 0.5f;
        [SerializeField] private Easing.Info alphaEasing = null;
        [SerializeField] private BombControl bomb = null;

        private void Awake()
        {
            if (GroundMask < 0)
            {
                GroundMask = 1 << LayerMask.NameToLayer(GroundLayerName);
            }
        }

        private void OnEnable()
        {
            sprite.transform.localScale = Vector3.one * bomb.ExplosionRadius * 2;
        }

        private void LateUpdate()
        {
            var color = sprite.color;
            color.a = Easing.Ease(alphaEasing.begin, alphaEasing.end, timer.ElapsedTime, timer.Duration, alphaEasing.type);
            sprite.color = color;

            if (transform.parent != null)
            {
                Vector3 pos;
                CalculateGroundAt(transform.parent.position, out pos);
                pos.y += distanceFromBottom;
                transform.position = pos;
            }
            transform.forward = Up;
        }

        static private bool CalculateGroundAt(Vector3 pos, out Vector3 hitPos)
        {
            hitPos = Zero;
            RaycastHit hitInfo;
            if (Physics.Raycast(new Vector3(pos.x, HitTestHeight, pos.z), Down, out hitInfo, HitTestLength, GroundMask))
            {
                hitPos = hitInfo.point;
                return true;
            }

            return false;
        }
    }
}
