using UnityEngine;
using Mirror;

using static BombGame.Utils;

namespace BombGame
{
    public class BombControl : NetworkBehaviour
    {
        private const string WaterLayerName = "Water";

        static private string[] HitLayers = { "Character", "Bomb" };

        [SerializeField] private ExplosionInfo explosionInfo = null;
        public float ExplosionRadius => explosionInfo.Radius;

        [Header("components")]
        [SerializeField] private CachedElem cached = null;
        public CachedElem Cached => cached;
        [SerializeField] private Rigidbody body = null;
        [SerializeField] new private Collider collider = null;
        [SerializeField] private BombRadiusSprite bombRadius = null;
        [SerializeField] private GrabTarget grabTarget = null;

        private int explosionMask;
        private Collider[] hitBuffer;

        public bool DidExplode { get; private set; }

        private bool explosionDisabled = false;

        private void Awake()
        {
            explosionMask = CalcLayerMask(HitLayers);
            hitBuffer = new Collider[32];

            grabTarget.OnPickedUp.AddListener((_) => 
            {
                SetGrabbedState();
            });
            grabTarget.OnDropped.AddListener(() => 
            {
                SetDroppedState();
            });
        }

        private void OnEnable()
        {
            DidExplode = false;
            SetDroppedState();
            bombRadius?.gameObject.SetActive(true);
            explosionDisabled = false;
        }

        private void SetDroppedState()
        {
            body.isKinematic = false;
            collider.enabled = true;
        }

        private void SetGrabbedState()
        {
            body.isKinematic = true;
            collider.enabled = false;
        }

        public void OnExplosion(Vector3 center, ExplosionInfo explosionInfo)
        {
            if (!DidExplode)
            {
                Explode();
            }
        }

        public void Explode()
        {
            if (!DidExplode)
            {
                DidExplode = true;

                if (isServer)
                {
                    Explosion();
                }
                else
                if (hasAuthority)
                {
                    CmdExplode();
                }
                else
                {
                    // not a server object, not authorized, should it do anything?
                }
            }
        }

        private void Unspawn()
        {
            cached.Game.UnspawnObject(gameObject);
        }

        [Command]
        private void CmdExplode()
        {
            if (!DidExplode)
            {
                DidExplode = true;
                Explosion();
            }
        }

        private void Explosion()
        {
            grabTarget.Drop();

            if (!explosionDisabled)
            {
                var game = cached.Game;
                game.Level.EraseBushAt(transform.position, ExplosionRadius);
                game.Level.EraseIceAt(transform.position, ExplosionRadius);

                HitObjectsInRadius();
                game.SpawnObject(ElemTheme.Type.Explosion, transform.position);
            }

            Unspawn();
        }

        private void HitObjectsInRadius()
        {
            int hitNum = Physics.OverlapSphereNonAlloc(transform.position, explosionInfo.Radius, hitBuffer, explosionMask);
            if (hitNum > 0)
            {
                for (int i = 0; i < hitNum; ++i)
                {
                    if (hitBuffer[i] != null && hitBuffer[i].gameObject != gameObject)
                    {
                        var explosionTarget = hitBuffer[i].GetComponent<ExplosionTarget>();
                        explosionTarget?.Exploded(transform.position, explosionInfo);
                    }

                    hitBuffer[i] = null;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(WaterLayerName))
            {
                explosionDisabled = true;
                bombRadius?.gameObject.SetActive(false);
            }
        }
    }
}