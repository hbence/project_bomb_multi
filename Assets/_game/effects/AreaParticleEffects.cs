using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BombGame
{
    public class AreaParticleEffects : MonoBehaviour
    {
        [SerializeField] private ParticleSystem[] particles = null;
    
        public void SetArea(Rect rect)
        {
            foreach(var pSystem in particles)
            {
                var shape = pSystem.shape;
                shape.shapeType = ParticleSystemShapeType.Box;
                shape.scale = new Vector3(rect.width, 1, rect.height);
            }
        }
    }
}