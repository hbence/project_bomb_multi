﻿using UnityEngine;

namespace BombGame
{
    public class EffectControl : MonoBehaviour
    {
        public bool destroyAfterOver = false;
        public float duration = 1f;

        public AudioClipHandler sound;

        private float timer = 0f;
        public GameObject[] activate;

        public bool IsOver { get; set; }

        private void Update()
        {
            timer += Time.deltaTime;
            if (timer > duration)
            {
                IsOver = true;
                gameObject.SetActive(false);
                if (destroyAfterOver)
                {
                    Destroy(gameObject);
                }
            }
        }

        private void OnEnable()
        {
            Reset();
            sound?.Play();

            if (activate != null)
            {
                foreach (var go in activate)
                {
                    go.SetActive(true);
                }
            }
        }

        public void Reset()
        {
            timer = 0;
            IsOver = false;
        }
    }
}