﻿using UnityEngine;

namespace BombGame
{
    public class StretchEffect : MonoBehaviour
    {
        private const float DeltaLimit = 0.00001f;

        [SerializeField] private Transform root;
        [SerializeField] private Easing.IntervalMap speedToStretch;
        [SerializeField] private float lerpToTargetScale = 1f;

        private Vector3 prevPos;
        private float prevDelta;

        private void OnEnable()
        {
            prevPos = root.position;
            prevDelta = 0;
        }

        private void LateUpdate()
        {
            Vector3 pos = root.position;
            Vector3 delta = pos - prevPos;
            prevPos = pos;

            float targetDelta = delta.magnitude;
            float lerpedDelta = Mathf.Lerp(prevDelta, targetDelta, Time.deltaTime * lerpToTargetScale);
            prevDelta = lerpedDelta;

            if (lerpedDelta > DeltaLimit)
            {
                StretchAndSqueeze(lerpedDelta);

                if (targetDelta > DeltaLimit)
                {
                    Quaternion original = root.rotation;
                    root.forward = delta;
                    Quaternion rot = original * Quaternion.Inverse(root.rotation);
                    RotateChildren(rot);
                }
            }
            else
            {
                root.localScale = Vector3.one;
            }
        }

        private void RotateChildren(Quaternion rot)
        {
            for (int i = 0; i < root.childCount; ++i)
            {
                root.GetChild(i).rotation *= rot;
            }
        }

        private void StretchAndSqueeze(float delta)
        {
            float offset = speedToStretch.Get(delta);

            float stretch = 1 + offset;
            float squeeze = Mathf.Sqrt(1 / stretch);

            root.localScale = new Vector3(squeeze, squeeze, stretch);
        }
    }
}