﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BombGame
{
    public class WobbleUp : MonoBehaviour
    {
        [SerializeField] private float frequency = 1;
        [SerializeField] private float damping = 1;

        private HarmonicMotion.DampenedSpringMotionParams springParams;
        private Vector3 velocity;

        void Start()
        {
            springParams = HarmonicMotion.CalcDampedSpringMotionParams(damping, frequency);
        }

        private void OnEnable()
        {
            velocity = Vector3.zero;
        }

        void LateUpdate()
        {
            Vector3 up = transform.up;
            HarmonicMotion.Calculate(ref up, ref velocity, Vector3.up, springParams);
            transform.up = up;
        }
    }
}