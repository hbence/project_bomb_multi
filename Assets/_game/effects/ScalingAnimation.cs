﻿using UnityEngine;

namespace BombGame
{
	public class ScalingAnimation : MonoBehaviour
	{
		public enum ScaleAxis
		{
			X, Y, Z, All
		}

		public Easing.Info scalingInfo;

		public bool looping = true;
		public bool onceBackAndForth = false;
		public ScaleAxis scaleAxis = ScaleAxis.All;

		private Easing.Object easer;
		public bool debug = false;

		void Start()
		{
			easer = new Easing.Object(scalingInfo);
		}

		void Update()
		{
			if (debug)
			{
				debug = false;
				Reset();
			}

			if (looping)
			{
				easer.StepBackAndForth(Time.unscaledDeltaTime);
				transform.localScale = GetScale(easer.Value);
			}
			else if (onceBackAndForth)
			{
				if (!easer.HasPlayedOnce)
				{
					easer.StepBackAndForthOnce(Time.unscaledDeltaTime);
					transform.localScale = GetScale(easer.Value);
				}
			}
			else
			{
				if (!easer.IsAtEnd)
				{
					easer.Step(Time.unscaledDeltaTime);
					transform.localScale = GetScale(easer.Value);
				}
			}
		}

		public void Reset()
		{
			if (scalingInfo != null)
			{
				if (easer == null)
				{
					easer = new Easing.Object(scalingInfo);
				}
				else
				{
					easer.Set(scalingInfo);
				}
			}
		}

		private void OnEnable()
		{
			Reset();
		}

		private Vector3 GetScale(float scale)
		{
			Vector3 s = transform.localScale;

			switch (scaleAxis)
			{
				case ScaleAxis.X: s.x = scale; break;
				case ScaleAxis.Y: s.y = scale; break;
				case ScaleAxis.Z: s.z = scale; break;
				case ScaleAxis.All: s.Set(scale, scale, scale); break;
			}

			return s;
		}
	}
}