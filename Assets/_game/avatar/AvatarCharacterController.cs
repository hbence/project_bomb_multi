using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;

using static BombGame.Utils;

namespace BombGame
{
    public class AvatarCharacterController : NetworkBehaviour
    {
        private static readonly Vector3 Up = Vector3.up;
        private static readonly Vector3 Zero = Vector3.zero;
        private const float TurnEpsilon = 0.0001f;

        private const string WaterLayerName = "Water";
        private const string BombLayerName = "Bomb";

        public enum CharacterState
        {
            Alive,
            Dead
        }

        [SerializeField] private int maxLifeNum = 3;
        public int MaxLifeNum => maxLifeNum;

        [SerializeField] private float hurtInvinciblityDuration = 1f;
        public float HurtInvincibilityDuration => hurtInvinciblityDuration;

        [Header("components")]
        [SerializeField] private CharacterController characterController = null;
        [SerializeField] private Rigidbody body = null;
        [SerializeField] private GrabController grabber = null;
        [SerializeField] private CharacterAnimationControl animationControl = null;

        [Header("movement")]
        [SerializeField] private MovementHandler movement = null;
        [SerializeField] private float turnTowardsMoveDirMulitplier = 2f;
        [SerializeField] private float bombPushForce = 1f;

        [Header("throwing")]
        [SerializeField] private ThrowHandler thrower = null;

        [Header("network adjustment")]
        [SerializeField] private float positionAdjustmentLimit = 0.15f;

        public GameControl Game { get; private set; } = null;
        public CharacterUIControl UI { get; set; } = null;

        public bool IsMoving => movement.IsMoving;
        public Vector3 MovementInputDirection => movement.LastMovementDirection;
        public float MovementInputStrengthNormalized => movement.LastNormalizedInputStrength;

        private Vector2 movementInput;

        private Vector3 lastPosition;

        private CharacterState state;
        public bool IsAlive => state == CharacterState.Alive;
        public bool IsDead => state == CharacterState.Dead;

        private Controls controls;

        private int bombLayer;

        [SyncVar(hook = "OnLifeNumChanged")]
        public int lifeNum;
        private float invincibilityTimer = 0;
        public bool IsInvincible => invincibilityTimer > 0;

        private void Awake()
        {
            controls = new Controls();
            controls.Player.CreateBombThrow.started += (_) => CmdStartChargingThrow();
            controls.Player.CreateBombThrow.canceled += (_) => CmdThrow(grabber.GrabbedPosition, grabber.GrabbedForward, grabber.GrabbedUp);
            controls.Player.PickupObject.started += (_) => CmdTryToGrab();

            // only enabled if has authority
            controls.Disable();
        
            bombLayer = LayerMask.NameToLayer(BombLayerName);
        }

        public override void OnStartLocalPlayer()
        {
            Game.AddCameraToPlayer(this);
        }

        public override void OnStartAuthority()
        {
            controls.Enable();
            base.OnStartAuthority();
        }

        public override void OnStopAuthority()
        {
            controls.Disable();
            base.OnStopAuthority();
        }

        public void Spawned(GameControl game, Vector3 pos)
        {
            Game = game;
            state = CharacterState.Alive;
            transform.rotation = Quaternion.identity;
            transform.position = pos;
            body.isKinematic = true;
            characterController.enabled = true;
            lifeNum = MaxLifeNum;

            if (UI != null)
            {
                UI.MaxLifeIconCount = MaxLifeNum;
                UI.FullLifeIconCount = lifeNum;
            }

            animationControl.ShouldAnimate = true;

            movement.Reset();
            grabber.Clear();
            thrower.Reset();

            lastPosition = transform.position;
        }

        private void OnEnable()
        {
            if (hasAuthority)
            {
                controls.Enable();
            }

            movement.Reset();
            thrower.Reset();

            lastPosition = transform.position;
        }

        private void OnDisable()
        {
            controls.Disable();
        }

        private void OnDestroy()
        {
            Game?.UI?.RemoveCharacter(this);
        }

        void Update()
        {
            float dt = Mathf.Clamp(Time.deltaTime, 0.003f, 0.1f);

            if (hasAuthority)
            {
                if (IsAlive)
                {
                    PollMovementInput();
                }

                var move = movement.Update(dt);

                if (characterController.enabled)
                {
                    characterController.Move(move);
                }
            }

            if (IsAlive)
            {
                if (invincibilityTimer > 0)
                {
                    invincibilityTimer -= dt;
                }

                thrower.Update(dt);

                HandleTurning(dt);

                lastPosition = transform.position;
            }
        }

        private void HandleTurning(float dt)
        {
            Vector3 dir = transform.position - lastPosition;
            dir.y = 0;
            if (dir.sqrMagnitude > TurnEpsilon)
            {
                Quaternion towards = Quaternion.LookRotation(dir, Up);
                transform.rotation = Quaternion.Lerp(transform.rotation, towards, dt * turnTowardsMoveDirMulitplier);
            }
            else
            {
                if (transform.forward.y != 0)
                {
                    transform.rotation = Quaternion.LookRotation(transform.forward, Up);
                }
            }
        }

        private void PollMovementInput()
        {
            //Debug.Log($"phase:{controls.Player.Move.phase} deta:{controls.Player.Move.ReadValue<Vector2>()}");

            var moveAction = controls.Player.Move;
            if (moveAction.phase == InputActionPhase.Started || moveAction.phase == InputActionPhase.Performed)
            {
                var delta = controls.Player.Move.ReadValue<Vector2>();
                if (delta.x != 0 || delta.y != 0)
                {
                    movement.Move(delta, delta.magnitude);
                }
                else
                {
                    movement.StopMoving();
                }
            }
            else
            {
                movement.StopMoving();
            }
        }

        public void OnExplosion(Vector3 center, ExplosionInfo explosionInfo)
        {
            Hurt();

            var force = explosionInfo.CalcPushForceVector(transform.position, center);
            PushByForce(force);
            RpcPushedByForce(force);
        }

        [ClientRpc]
        private void RpcPushedByForce(Vector3 force)
        {
            if (isClientOnly)
            {
                PushByForce(force);
            }
        }

        private void PushByForce(Vector3 force)
        {
            if (body.isKinematic)
            {
                movement.PushForce += force;
            }
            else
            {
                body.AddForce(force, ForceMode.Impulse);
            }
        }

        public void Hurt()
        {
            if (!IsAlive)
            {
                return;
            }

            if (!IsInvincible)
            {
                --lifeNum;

                if (UI != null)
                {
                    UI.FullLifeIconCount = lifeNum;
                }

                if (lifeNum <= 0)
                {
                    Die();
                }
                else
                {
                    invincibilityTimer = HurtInvincibilityDuration;
                }
            }
        }

        private void OnLifeNumChanged(int oldValue, int newValue)
        {
            if (UI != null)
            {
                UI.FullLifeIconCount = newValue;
            }
        }

        public void Die()
        {
            if (IsAlive)
            {
                MakeDead();
                
                if (isServer)
                {
                    RpcDied();
                }
            }
        }

        [ClientRpc]
        private void RpcDied()
        {
            MakeDead();
        }

        private void MakeDead()
        {
            lifeNum = 0;

            state = CharacterState.Dead;
            body.isKinematic = false;
            characterController.enabled = false;

            animationControl.ShouldAnimate = false;

            body.AddTorque(15, 0, 0);
        }

        [Command]
        public void CmdSpawnBomb()
        {
            SpawnBomb();
        }

        private void SpawnBomb()
        {
            grabber.Drop();

            var bomb = Game.SpawnObject(ElemTheme.Type.NormalBomb, grabber.GrabbedPosition);

            var grabComponent = bomb.GetComponent<GrabTarget>();
            grabber.Grab(grabComponent);

            var bombId = bomb.GetComponent<NetworkIdentity>();
            RpcGrabbed(bombId);
        }

        [Command]
        public void CmdStartChargingThrow()
        {
            if (!IsAlive)
            {
                return;
            }

            if (!grabber.IsGrabbing)
            {
                SpawnBomb();
            }

            thrower.StartCharging();
            RpcStartChargingThrow();
        }

        [ClientRpc]
        public void RpcStartChargingThrow()
        {
            thrower.StartCharging();
        }

        [Command]
        public void CmdThrow(Vector3 pos, Vector3 forward, Vector3 right)
        {
            if (!IsAlive)
            {
                return;
            }

            pos = LimitPosition(grabber.GrabbedPosition, pos);
            Throw(pos, forward, right);
            RpcThrown();
        }

        private Vector3 LimitPosition(Vector3 localPosition, Vector3 clientPosition)
        {
            Vector3 delta = clientPosition - localPosition;
            if (delta.magnitude > positionAdjustmentLimit)
            {
                delta.Normalize();
                delta *= positionAdjustmentLimit;

                clientPosition = localPosition + delta;
            }

            return clientPosition;
        }

        private static float CalcHorizontalAngle(Vector3 forward)
            => Mathf.Atan2(-forward.z, forward.x) + Mathf.PI * 0.5f;
        
        private static Vector3 CalcHorizontalVector(float angle)    
            => new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle));

        [ClientRpc]
        public void RpcThrown()
        {
            var grabbedObj = grabber.GrabbedObject;
            if (grabbedObj != null)
            {
                // this clears the position interpolation in the network transform
                grabbedObj.gameObject.SetActive(false);
                grabbedObj.gameObject.SetActive(true);
                Throw(grabber.GrabbedPosition, transform.forward, transform.up);
            }
        }

        private void Throw(Vector3 from, Vector3 forward, Vector3 up)
        {
            if (grabber.IsGrabbing)
            {
                var grabbedBody = grabber.GrabbedObject.GetComponent<Rigidbody>();
                grabber.Drop();

                if (grabbedBody)
                {
                    grabbedBody.transform.position = from;
                    grabbedBody.position = from;
                    thrower.Throw(grabbedBody, forward, Vector3.Cross(up, forward));
                }
            }
        }

        [Command]
        public void CmdTryToGrab()
        {
            if (!IsAlive)
            {
                return;
            }

            if (!grabber.IsGrabbing)
            {
                var grabbedObj = grabber.FindGrabTarget();
                if (grabbedObj != null)
                {
                    grabber.Grab(grabbedObj);

                    var grabbedId = grabbedObj.GetComponent<NetworkIdentity>();
                    if (grabbedId != null)
                    {
                        RpcGrabbed(grabbedId);
                    }
                }
            }
        }

        [ClientRpc]
        public void RpcGrabbed(NetworkIdentity obj)
        {
            var grabObject = obj.GetComponent<GrabTarget>();
            grabber.Grab(grabObject);
        }

        [ClientRpc]
        public void RpcDropped()
        {
            grabber.Drop();
        }

        public void OnDrawGizmos()
        {
            thrower?.OnDrawDebug(transform.position, transform.forward, transform.right);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(WaterLayerName))
            {
                Die();
            }
        }

        [ServerCallback]
        public void OnControllerColliderHit(ControllerColliderHit hit)
        {
            var go = hit.gameObject;
            if (go.layer == bombLayer)
            {
                PushBomb(go);
            }
        }
        /*
        [ServerCallback]
        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.layer == bombLayer)
            {
                PushBomb(collision.gameObject);
            }
        }
        */
        private void PushBomb(GameObject go)
        {
            Rigidbody body = go.GetComponent<Rigidbody>();
            if (body != null)
            {
                Vector3 force = body.position - transform.position;
                force.Normalize();
                force *= bombPushForce;
                body.AddForce(force, ForceMode.Impulse);
            }
        }

        [System.Serializable]
        private class MovementHandler
        {
            [SerializeField] private float minMovementSpeed = 1f;
            [SerializeField] private float maxMovementSpeed = 5f;
            [SerializeField] private float movementDuration = 1f;
            [SerializeField] private Easing.Type movementEaseType = Easing.Type.OutCubic;
            [SerializeField] private float stopDrag = 0.9f;
            [SerializeField] private float gravity = 10f;
            [SerializeField] private float drag = 0.03f;
            [SerializeField] private float pushForceMultiplier = 2f;

            public bool IsMoving { get; private set; } = false;

            public Vector3 LastMovementDirection { get; private set; } = new Vector3();
            public float LastNormalizedInputStrength { get; private set; }

            private Easing.Object movementEaser;
            private Vector3 movementVelocity;

            public Vector3 PushForce { get; set; }

            public void Reset()
            {
                if (movementEaser == null)
                {
                    movementEaser = new Easing.Object(minMovementSpeed, maxMovementSpeed, movementDuration, movementEaseType);
                }

                movementEaser.Reset();

                IsMoving = false;
                movementVelocity = Vector3.zero;

                LastMovementDirection = Vector3.zero;
                LastNormalizedInputStrength = 0;
            }

            public Vector3 Update(float dt)
            {
                if (IsMoving)
                {
                    movementEaser.Step(dt);
                }
                else
                {
                    movementVelocity *= stopDrag;
                }

                Vector3 movement = movementVelocity;
                movement.y = -gravity;
                movement *= dt;

                movement += PushForce * dt * pushForceMultiplier;
                PushForce *= 1f - drag;

                return movement;
            }

            public void Move(Vector2 direction, float normalizedStrength)
            {
                direction.Normalize();
                normalizedStrength = Mathf.Clamp01(normalizedStrength);

                if (normalizedStrength > 0)
                {
                    IsMoving = true;
                    float force = normalizedStrength * movementEaser.Value;
                    movementVelocity.x = direction.x * force;
                    movementVelocity.z = direction.y * force;
                }

                LastMovementDirection = new Vector3(direction.x, 0, direction.y);
                LastNormalizedInputStrength = normalizedStrength;
            }

            public void StopMoving()
            {
                IsMoving = false;
                movementEaser.Reset();
            }
        }

        [System.Serializable]
        private class ThrowHandler
        {
            [SerializeField] private float minThrowForce = 5f;
            [SerializeField] private float maxThrowForce = 10f;
            [SerializeField] private float throwChargeDuration = 4f;
            [SerializeField] private float throwAngle = 10f;

            private Timer throwChargeTimer;
            public bool IsCharging { get; private set; }

            public void Reset()
            {
                if (throwChargeTimer == null)
                {
                    throwChargeTimer = new Timer(throwChargeDuration);
                }
                else
                {
                    throwChargeTimer.Duration = throwChargeDuration;
                    throwChargeTimer.Reset();
                }
                IsCharging = false;
            }

            public void Update(float dt)
            {
                if (IsCharging)
                {
                    throwChargeTimer.Step(dt);
                }
            }

            public void StartCharging()
            {
                Reset();
                IsCharging = true;
            }

            public void Throw(Rigidbody body, Vector3 forward, Vector3 right)
            {
                float forceMagnitude = Mathf.Lerp(minThrowForce, maxThrowForce, throwChargeTimer.Progress);
                forward.y = 0;
                Vector3 force = Quaternion.AngleAxis(-throwAngle, right) * forward;
                force.Normalize();
                force *= forceMagnitude;
                body.AddForce(force, ForceMode.Impulse);

                IsCharging = false;
            }

            public void OnDrawDebug(Vector3 pos, Vector3 forward, Vector3 right)
            {
                if (throwChargeTimer != null)
                {
                    float forceMagnitude = Mathf.Lerp(minThrowForce, maxThrowForce, throwChargeTimer.Progress);
                    forward.y = 0;
                    Vector3 force = Quaternion.AngleAxis(-throwAngle, right) * forward;
                    force.Normalize();
                    force *= forceMagnitude;

                    Debug.DrawLine(pos, pos + force);
                }
            }
        }
    }
}
