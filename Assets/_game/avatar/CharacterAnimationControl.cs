using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static BombGame.CharacterAnimationControl.AnimatorVariables.Name;

namespace BombGame
{
    public class CharacterAnimationControl : MonoBehaviour
    {
        private static readonly Vector3 Up = Vector3.up;
        private static readonly Vector3 Down = Vector3.down;
        private static readonly Quaternion Identity = Quaternion.identity;

        private const string GroundLayerName = "Ground";
        private static int GroundMask = -1;

        [SerializeField] private Animator animator = null;
        [SerializeField] private float groundCheckDistance = 1.5f;
       // [SerializeField] private Vector3 groundBox = Vector3.one;
        [SerializeField] private float animatorSpeedMultiplier = 1f;

        public bool ShouldAnimate { get; set; } = true;

        private AnimatorVariables animatorVars;

        private bool wasOnGround;
        private bool isOnGround;
        private Vector3 lastPostion;

        void Awake()
        {
            if (GroundMask < 0)
            {
                GroundMask = 1 << LayerMask.NameToLayer(GroundLayerName);
            }

            animatorVars = new AnimatorVariables(animator);
        }

        private void OnEnable()
        {
            wasOnGround = false;
            isOnGround = false;
            lastPostion = transform.position;
        }

        void Update()
        {
            Vector3 delta = transform.position - lastPostion;

            if (ShouldAnimate)
            {
                isOnGround = DoesHitGround(transform.position);

                animatorVars.SetBool(Grounded, isOnGround);
                animatorVars.SetFloat(MoveSpeed, delta.magnitude * animatorSpeedMultiplier);

                if (!wasOnGround && isOnGround)
                {
                    animatorVars.SetTrigger(Land);
                }

                if (wasOnGround && !isOnGround)
                {
                    animatorVars.SetTrigger(Jump);
                }

                if (Input.GetKey(KeyCode.Return))
                {
                    animatorVars.SetTrigger(Pickup);
                }
                if (Input.GetKeyDown(KeyCode.Backspace))
                {
                    animatorVars.SetTrigger(Push);
                }
            }

            wasOnGround = isOnGround;
            lastPostion = transform.position;
        }

        private bool DoesHitGround(Vector3 pos)
            => Physics.Raycast(pos, Down, groundCheckDistance, GroundMask);
            //=> Physics.BoxCast(pos, groundBox, Down, Identity, groundCheckDistance, GroundMask);

        public class AnimatorVariables
        {
            public enum Name
            {
                MoveSpeed = 0,
                Jump,
                Land,
                Pickup,
                Grounded,
                Wave,
                Climb,
                Push
            }

            private int[] ids;
            private Animator Animator { get; }

            public AnimatorVariables(Animator animator)
            {
                Animator = animator;

                var names = System.Enum.GetNames(typeof(Name));
                int count = names.Length;
                ids = new int[count];
                for (int i = 0; i < count; ++i)
                {
                    ids[i] = Animator.StringToHash(names[i]);
                }
            }

            public int GetId(Name name)
                => ids[(int)name];

            public void SetFloat(Name name, float value)
                => Animator.SetFloat(GetId(name), value);

            public void SetBool(Name name, bool value)
                => Animator.SetBool(GetId(name), value);

            public void SetTrigger(Name name)
                => Animator.SetTrigger(GetId(name));
        }
    }
}
