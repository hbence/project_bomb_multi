using UnityEngine;

namespace BombGame
{
    public class CharacterCameraControl : MonoBehaviour
    {
        static private readonly Vector3 Zero = Vector3.zero;
        static private readonly Quaternion Identity = Quaternion.identity;

        [SerializeField] private FollowingCameraControl cam = null;
        [SerializeField] private AvatarCharacterController character = null;
        public AvatarCharacterController Character
        {
            get => character;
            set
            {
                character = value;
                cam.Target = value != null ? value.transform : null;
            }
        }
        [SerializeField] private ShakeableTransform shakableTransform = null;
        public ShakeableTransform ShakableTransform => shakableTransform;

        [Header("settings")]
        [SerializeField] private float lookUpNormalized = 0.15f;
        [SerializeField] private float lookDownNormalized = 0.15f;
        [SerializeField] private float lookSideNormalized = 0.15f;
        [SerializeField] private float lookAheadMultiplier = 3f;
        [SerializeField] private float backToCenterMultiplier = 3f;
        [SerializeField] private float movingForwardVerticalAngle = 55;
        [SerializeField] private float defVerticalAngle = 55;
        [SerializeField] private float movingBackwardVerticalAngle = 65;

        void LateUpdate()
        {
            if (character != null)
            {
                UpdateLookAhead();
            }

            CheckShakeTransform();
        }

        private void CheckShakeTransform()
        {
            if (shakableTransform != null && !shakableTransform.IsShaking)
            {
                shakableTransform.transform.localPosition = Zero;
                shakableTransform.transform.localRotation = Identity;
            }
        }

        public void UpdateLookAhead()
        {
            float t = Time.deltaTime * lookAheadMultiplier;
            float centerX = cam.ViewCenterX;
            float centerY = cam.ViewCenterY;
            float vcAngle = cam.VerticalAngle;
            if (character.IsMoving)
            {
                Vector3 moveDir = character.MovementInputDirection * character.MovementInputStrengthNormalized;
                centerX = Mathf.Lerp(centerX, 0.5f - moveDir.x * lookSideNormalized, t);
                float verticalOffsetNormalized = Mathf.Lerp(lookDownNormalized, lookUpNormalized, (moveDir.z + 1) * 0.5f);
                centerY = Mathf.Lerp(centerY, 0.5f - moveDir.z * verticalOffsetNormalized, t);

                float angleTarget = Mathf.Lerp(movingBackwardVerticalAngle, movingForwardVerticalAngle, (moveDir.z + 1f) * 0.5f);
                vcAngle = Mathf.Lerp(vcAngle, angleTarget, t);
            }
            else
            {
                centerX = Mathf.Lerp(centerX, 0.5f, t);
                centerY = Mathf.Lerp(centerY, 0.5f, t);

                vcAngle = Mathf.Lerp(vcAngle, defVerticalAngle, Time.deltaTime * backToCenterMultiplier);
            }
            cam.ViewCenterX = centerX;
            cam.ViewCenterY = centerY;
            cam.VerticalAngle = vcAngle;
        }

        public void SnapToTarget()
            => cam.SnapToTarget();

        private static void LerpTo(ref float val, float target, float t)
            => val = Mathf.Lerp(val, target, t);
    }
}