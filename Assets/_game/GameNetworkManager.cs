using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace BombGame
{
    public class GameNetworkManager : NetworkManager
    {
        static public GameNetworkManager Instance { get; private set; }
        static public ClientInstance LocalClientInstance => Instance.LocalClient;

        [Header("game")]
        [SerializeField] private GameControl game = null;
        [SerializeField] private ElemTheme.Type[] spawnableElems = null;

        public ClientInstance LocalClient { get; private set; }

        private List<ClientInstance> clients;

        public override void Awake()
        {
            Instance = this;

            base.Awake();
        }

        public override void Start()
        {
            clients = new List<ClientInstance>();
            
            base.Start();
        }

        public override void OnStartServer()
        {
            startPositions.Clear();
            startPositions.AddRange(game.Level.SpawnPoints);

            base.OnStartServer();
        }

        public override void OnStartClient()
        {
            var avatarPrefab = game.ElemTheme.GetPrefab(ElemTheme.Type.PlayerAvatar);
            NetworkClient.RegisterPrefab(avatarPrefab, SpawnAvatar, UnspawnAvatar);
            
            base.OnStartClient();

            NetworkClient.UnregisterPrefab(playerPrefab);
            NetworkClient.RegisterPrefab(playerPrefab, SpawnClient, UnspawnClient);

            foreach(var elem in spawnableElems)
            {
                var prefab = game.ElemTheme.GetPrefab(elem);
                NetworkClient.RegisterPrefab(prefab, 
                    (SpawnMessage msg) => game.SpawnObjectLocal(elem, msg.position), 
                    (GameObject go) => game.UnspawnObject(go));
            }
        }

        public override void OnServerAddPlayer(NetworkConnection conn)
        {
            Transform startPos = GetStartPosition();
            GameObject player = startPos != null
                ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
                : Instantiate(playerPrefab);


            var client = player.GetComponent<ClientInstance>();
            client.Game = game;
            clients.Add(client);

            if (NetworkClient.localPlayer == conn.identity)
            {
                LocalClient = client;
            }

            // instantiating a "Player" prefab gives it the name "Player(clone)"
            // => appending the connectionId is WAY more useful for debugging!
            player.name = $"player:{playerPrefab.name} [connId={conn.connectionId}]";
            NetworkServer.AddPlayerForConnection(conn, player);
        }

        private GameObject SpawnClient(SpawnMessage msg)
        {
            GameObject go = Instantiate(playerPrefab);
            var client = go.GetComponent<ClientInstance>();
            client.Game = game;
            clients.Add(client);
            
            return go;
        }

        private void UnspawnClient(GameObject go)
        {
            var client = go.GetComponent<ClientInstance>();
            if (client != null)
            {
                clients.Remove(client);
            }
            else
            {
                Debug.LogWarning("GameObject has no clientInstance!");
            }
            DestroyImmediate(go);
        }

        private GameObject SpawnAvatar(SpawnMessage msg)
        { 
            var go = game.SpawnAvatar(msg.position);
            var avatar = go.GetComponent<AvatarCharacterController>();

            if (msg.isOwner)
            {
                game.AddCameraToPlayer(avatar);
            }
            
            return go;
        }
        private void UnspawnAvatar(GameObject go)
            => game.UnspawnAvatar(go);
    }
}