using UnityEngine;
using UnityEngine.UI;

namespace BombGame
{
    public class CharacterUIControl : MonoBehaviour
    {
        [SerializeField] private RectTransform rect = null;
        public RectTransform Rect => rect;
        [SerializeField] private TMPro.TextMeshProUGUI nameLabel = null;
        [SerializeField] private Image[] healthIcons = null;
        [SerializeField] private HealthIconInfo fullInfo = null;
        [SerializeField] private HealthIconInfo emptyInfo = null;
        [SerializeField] private float animationDuration = 1f;
        [SerializeField] private Easing.Type animationType = Easing.Type.OutCubic;

        public string Name
        {
            get => nameLabel.text;
            set
            {
                nameLabel.text = value;
            }
        }

        private int maxIconCount;
        public int MaxLifeIconCount
        {
            get => maxIconCount;
            set
            {
                maxIconCount = value;
                if (maxIconCount < 1)
                {
                    Debug.LogWarning("should be at least one");
                    maxIconCount = 1;
                }
                if (maxIconCount > healthIcons.Length)
                {
                    Debug.LogWarning("not enough health icons");
                    maxIconCount = healthIcons.Length;
                }
                for (int i = 0; i < healthIcons.Length; ++i)
                {
                    bool active = i < maxIconCount;
                    var icon = healthIcons[i];
                    icon.gameObject.SetActive(active);
                    fullInfo.Apply(icon);
                }
            }
        }

        private int fullIconCount;
        public int FullLifeIconCount
        {
            get => fullIconCount;
            set
            {
                fullIconCount = Mathf.Clamp(value, 0, maxIconCount);
                if (infoArray != null)
                {
                    for (int i = 0; i < MaxLifeIconCount; ++i)
                    {
                        infoArray[i].IsFull = i < fullIconCount;
                    }
                }
            }
        }

        private Info[] infoArray;

        void Start()
        {
            infoArray = new Info[healthIcons.Length];
            for (int i = 0; i < infoArray.Length; ++i)
            {
                infoArray[i] = new Info(healthIcons[i]);
            }

            MaxLifeIconCount = maxIconCount;
            FullLifeIconCount = fullIconCount;
            FinishAnimation();
        }

        private void OnEnable()
        {
            FullLifeIconCount = fullIconCount;
            if (infoArray != null)
            {
                foreach (var info in infoArray)
                {
                    info.FinishAnimation(animationDuration, emptyInfo, fullInfo);
                }
            }
        }

        void Update()
        {
            foreach(var info in infoArray)
            {
                info.Update(Time.deltaTime, animationDuration, emptyInfo, fullInfo, animationType);
            }
        }

        public void FinishAnimation()
        {
            foreach (var info in infoArray)
            {
                info.FinishAnimation(animationDuration, emptyInfo, fullInfo);
            }
        }

        private class Info
        {
            private Image Image { get; }

            private float time;
            
            private bool isFull;
            public bool IsFull
            {
                get => isFull;
                set
                {
                    if (isFull != value)
                    {
                        isFull = value;
                        time = 0;
                    }
                }
            }

            public Info(Image image)
            {
                Image = image;
            }

            public void Update(float dt, float duration, HealthIconInfo empty, HealthIconInfo full, Easing.Type easing)
            {
                if (time < duration)
                {
                    time = Mathf.Min(time + dt, duration);
                    var (from, to) = isFull ? (empty, full) : (full, empty);
                    HealthIconInfo.Lerp(Image, from, to, time, duration, easing);
                }
            }

            public void FinishAnimation(float duration, HealthIconInfo empty, HealthIconInfo full)
            {
                time = duration;
                var info = IsFull ? full : empty;
                info.Apply(Image);
            }
        }

        [System.Serializable]
        private class HealthIconInfo
        {
            [SerializeField] private float size;
            [SerializeField] private Color color;

            public void Apply(Image icon)
            {
                icon.color = color;
                icon.rectTransform.sizeDelta = new Vector2(size, size);
            }

            static public void Lerp(Image icon, HealthIconInfo from, HealthIconInfo to, float time, float duration, Easing.Type easing)
            {
                float easedT = Easing.Ease(0, 1, time, duration, easing);
                icon.color = new Color(
                    Mathf.Lerp(from.color.r, to.color.r, easedT),
                    Mathf.Lerp(from.color.g, to.color.g, easedT),
                    Mathf.Lerp(from.color.b, to.color.b, easedT),
                    Mathf.Lerp(from.color.a, to.color.a, easedT)
                );

                float size = Mathf.Lerp(from.size, to.size, easedT);
                icon.rectTransform.sizeDelta = new Vector2(size, size);
            }
        }
    }
}