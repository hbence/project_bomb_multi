using System.Collections.Generic;
using UnityEngine;

using static BombGame.Utils;

namespace BombGame
{
    public class UIControl : MonoBehaviour
    {
        [SerializeField] private GameObject characterUIPrefab = null;
        [SerializeField] private Camera cam = null;
        [SerializeField] private Canvas canvas = null;

        private List<CharacterUI> characterUIs;
        private RectTransform characterUIRoot;

        void Awake()
        {
            characterUIs = new List<CharacterUI>();

            characterUIRoot = new GameObject("character_ui_root").AddComponent<RectTransform>();
            characterUIRoot.SetParent(transform);
            characterUIRoot.localScale = Vector3.one;
            characterUIRoot.anchorMin = Vector2.zero;
            characterUIRoot.anchorMax = Vector2.zero;
            characterUIRoot.anchoredPosition = Vector2.zero;
        }

        void Update()
        {
            foreach(var ui in characterUIs)
            {
                ui.PlaceUI(cam, canvas);
            }
        }

        public void AddCharacter(AvatarCharacterController character)
        {
            if (characterUIs.Exists(UIWithCharacter(character)))
            {
                Debug.LogWarning("trying to add a character multiple times!");
            }
            else
            {
                var charUI = new CharacterUI(character, characterUIPrefab, characterUIRoot);
                characterUIs.Add(charUI);
            }
        }

        public void RemoveCharacter(AvatarCharacterController character)
        {
            characterUIs.ForEach((CharacterUI charUI) => { if (HasCharacter(charUI, character)) { charUI.Destroy(); } });
            characterUIs.RemoveAll(UIWithCharacter(character));
        }

        static private bool HasCharacter(CharacterUI charUi, AvatarCharacterController avatar) => charUi.Avatar == avatar;
        static private System.Predicate<CharacterUI> UIWithCharacter(AvatarCharacterController avatar) => (CharacterUI ui) => ui.Avatar == avatar;

        private class CharacterUI
        {
            public CharacterUIControl UI { get; }
            public AvatarCharacterController Avatar { get; }

            public CharacterUI(AvatarCharacterController avatar, GameObject characterUIPrefab, Transform uiRoot)
            {
                Avatar = avatar;
                UI = CreateUI(characterUIPrefab, uiRoot);

                Avatar.UI = UI;
            }

            public void PlaceUI(Camera cam, Canvas canvas)
            {
                var worldPos = Avatar.transform.position;

                var screenPos = cam.WorldToScreenPoint(worldPos);
                bool isOnScreen = IsOnScreen(screenPos);
                SetActive(UI, isOnScreen);
                if (IsActive(UI))
                {
                    screenPos.x /= canvas.transform.localScale.x;
                    screenPos.y /= canvas.transform.localScale.y;
                    
                    UI.Rect.anchoredPosition = screenPos;
                }
            }

            public bool IsOnScreen(Vector3 screenPos)
                => screenPos.x > 0 && screenPos.y > 0 && screenPos.z < Screen.width && screenPos.y < Screen.height;

            private CharacterUIControl CreateUI(GameObject prefab, Transform uiRoot)
            {
                var go = Instantiate(prefab, uiRoot);
                return go.GetComponent<CharacterUIControl>();
            }

            public void Destroy()
            {
                if (Avatar.UI == UI)
                {
                    Avatar.UI = null;
                }
                if (UI)
                {
                    DestroyImmediate(UI.gameObject);
                }
            }
        }
    }
}