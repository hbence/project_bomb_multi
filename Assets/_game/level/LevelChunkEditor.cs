using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MeshBuilder;
using static BombGame.LevelChunk;
using static MeshBuilder.MarchingSquaresMesher;

namespace BombGame
{
    [ExecuteInEditMode]
    public class LevelChunkEditor : MonoBehaviour
    {
        [SerializeField] private string binaryPath = "";
        public string BinaryStreamingPath => Application.streamingAssetsPath + "/" + binaryPath;
        [SerializeField] private MarchingSquaresEditorComponent groundEditor = null;
        [SerializeField] private MarchingSquaresEditorComponent iceEditor = null;
        [SerializeField] private MarchingSquaresEditorComponent bushEditor = null;
        [SerializeField] private MarchingSquaresEditorComponent cliffEditor = null;

        public bool save = false;
        public bool load = false;

        private void Update()
        {
            if (save)
            {
                save = false;
                Save();
            }

            if (load)
            {
                load = false;
                Load();
            }
        }

        public void Save()
        {
            groundEditor.DataComponent.Save();
            iceEditor.DataComponent.Save();
            bushEditor.DataComponent.Save();
            cliffEditor.DataComponent.Save();

            using Data ground = groundEditor.DataComponent.LoadData();
            using Data ice = iceEditor.DataComponent.LoadData();
            using Data bush = bushEditor.DataComponent.LoadData();
            using Data cliff = cliffEditor.DataComponent.LoadData();
            ChunkData.SaveBinary(BinaryStreamingPath, ground, ice, bush, cliff);
        }

        public void Load()
        {
            using var data = ChunkData.LoadBinary(BinaryStreamingPath);

            UpdateData(groundEditor, data.GroundData);
            UpdateData(iceEditor, data.IceData);
            UpdateData(bushEditor, data.BushData);
            UpdateData(cliffEditor, data.CliffData);
        }

        static private void UpdateData(MarchingSquaresEditorComponent editor, Data data)
        {
            editor.DataComponent.UpdateData(data);
            
            if (!Application.isPlaying)
            {
                foreach (var ms in editor.Meshers)
                {
                    var mesh = new Mesh();
                    var mesher = new MarchingSquaresMesher();
                    ms.InitInfo.Init(mesher, data);
                    mesher.Start();
                    mesher.Complete(mesh);
                    ms.MeshFilter.sharedMesh = mesh;
                }
            }
        }
    }
}