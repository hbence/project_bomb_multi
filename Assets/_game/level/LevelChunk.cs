using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

using MeshBuilder;
using static MeshBuilder.MarchingSquaresMesher;
using static MeshBuilder.MarchingSquaresDataComponent;

namespace BombGame
{
    public class LevelChunk : MonoBehaviour
    {
        private const float LimitedDistanceMinimum = -0.5f;
        private const float LimitedDistanceMaximum = 0.5f;

        private static readonly Rect NullRect = new Rect(0, 0, 0, 0);

        [SerializeField] private string binaryPath = "";

        [SerializeField] private float cellSize = 1f; 
        [SerializeField] private LayerHandler groundLayer = null;
        [SerializeField] private LayerHandler iceLayer = null;
        [SerializeField] private LayerHandler bushLayer = null;
        [SerializeField] private LayerHandler cliffLayer = null;

        public LevelControl Level { get; set; }

        public ChunkData data;
        public Data BushData => data.BushData;
        public Data IceData => data.IceData;

        private List<BrushData> bushChanges;
        private List<UpdateData> bushUpdates;
        private List<BrushData> iceChanges;
        private List<UpdateData> iceUpdates;

        private void Awake()
        {
            bushChanges = new List<BrushData>();
            bushUpdates = new List<UpdateData>();
            iceChanges = new List<BrushData>();
            iceUpdates = new List<UpdateData>();
        }

        private void Start()
        {
            data = ChunkData.LoadBinary(Application.streamingAssetsPath + "/" + binaryPath);
            Init(data);
        }

        private void OnDestroy()
        {
            data?.Dispose();
            data = null;
        }

        public void Init(ChunkData data)
        {
            groundLayer.Init(transform, data.GroundData);
            iceLayer.Init(transform, data.IceData);
            bushLayer.Init(transform, data.BushData);
            cliffLayer.Init(transform, data.CliffData);

            groundLayer.StartGeneration();
            iceLayer.StartGeneration();
            bushLayer.StartGeneration();
            cliffLayer.StartGeneration();
        }

        public void EraseBushAt(Vector3 pos, float radius)
        {
            if (IsBrushOver(pos, radius))
            {
                pos -= transform.position;
                bushChanges.Add(new BrushData(pos, radius, BrushData.Type.Erase));
            }
        }

        private Rect CalcFilledDataAt(Vector3 pos, float radius, Data data)
        {
            if (IsBrushOver(pos, radius))
            {
                pos -= transform.position;

                RangeInt cols, rows;
                data.CalcAABBRanges(pos.x - radius, pos.z - radius, pos.x + radius, pos.z + radius, cellSize, out cols, out rows);

                Rect res = new Rect(cols.end, rows.end, cols.start, rows.start);

                for (int row = rows.start; row < rows.end; ++row)
                {
                    for (int col = cols.start; col < cols.end; ++col)
                    {
                        if (data.DistanceAt(col, row) > 0)
                        {
                            res.x = Mathf.Min(res.x, col);
                            res.y = Mathf.Min(res.y, row);
                            res.width = Mathf.Max(res.x, col);
                            res.height = Mathf.Max(res.y, row);
                        }
                    }
                }

                res.width -= res.x;
                res.height -= res.y;

                res.x *= cellSize;
                res.y *= cellSize;
                res.width *= cellSize;
                res.height *= cellSize;

                res.x += transform.position.x;
                res.y += transform.position.y;

                return res;
            }

            return NullRect;
        }

        public Rect CalcFilledIceDataAt(Vector3 pos, float radius)
            => CalcFilledDataAt(pos, radius, data.IceData);

        public Rect CalcFilledBushDataAt(Vector3 pos, float radius)
            => CalcFilledDataAt(pos, radius, data.BushData);

        public void UpdateBush(byte[] data)
        {
            bushUpdates.Add(new UpdateData(data));
        }

        public void EraseIceAt(Vector3 pos, float radius)
        {
            if (IsBrushOver(pos, radius))
            {
                pos -= transform.position;
                iceChanges.Add(new BrushData(pos, radius, BrushData.Type.Erase));
            }
        }

        public void UpdateIce(byte[] data)
        {
            iceUpdates.Add(new UpdateData(data));
        }

        public bool IsBrushOver(Vector3 pos, float radius)
            => !(pos.x + radius < PosX || pos.x - radius > PosX + Height ||
                 pos.z + radius < PosZ || pos.z - radius > PosZ + Width);

        private float PosX => transform.position.x;
        private float PosZ => transform.position.z;

        private float Width => data.GroundData.ColNum * cellSize;
        private float Height => data.GroundData.RowNum * cellSize;

        private void OnEnable()
        {
            Camera.onPreCull -= RenderWithCamera;
            Camera.onPreCull += RenderWithCamera;
        }

        private void OnDisable()
        {
            Camera.onPreCull -= RenderWithCamera;
        }

        private void Update()
        {
            if (HasChanges(iceChanges, iceUpdates))
            {
                RegenerateWithChanges(iceLayer, data.IceData, iceChanges, iceUpdates);
                iceChanges.Clear();
                iceUpdates.Clear();

                if (Level.isServer)
                {
                    Level.UpdatedIceLayerOnServer(this, EncodeLimitedDataDistances(data.IceData));
                }
            }

            if (HasChanges(bushChanges, bushUpdates))
            {
                RegenerateWithChanges(bushLayer, data.BushData, bushChanges, bushUpdates);
                bushChanges.Clear();
                bushUpdates.Clear();

                if (Level.isServer)
                {
                    Level.UpdatedBushLayerOnServer(this, EncodeLimitedDataDistances(data.BushData));
                }
            }
        }

        private bool HasChanges(List<BrushData> changes, List<UpdateData> updates)
            => changes.Count > 0 || updates.Count > 0;

        private void RegenerateWithChanges(LayerHandler layer, Data data, List<BrushData> changes, List<UpdateData> updates)
        {
            if (changes.Count > 0)
            {
                foreach (var change in changes)
                {
                    change.Apply(data, cellSize);
                }
            }

            if (updates.Count > 0)
            {
                foreach (var update in updates)
                {
                    update.Apply(data);
                }
            }

            layer.UpdateData(data);
            layer.StartGeneration();
        }

        private void LateUpdate()
        {
            groundLayer.EndGeneration();
            iceLayer.EndGeneration();
            bushLayer.EndGeneration();
            cliffLayer.EndGeneration();
        }

        private void RenderWithCamera(Camera cam)
        {
            groundLayer.Render(cam);
            iceLayer.Render(cam);
            bushLayer.Render(cam);
            cliffLayer.Render(cam);
        }

        static public byte[] EncodeLimitedDataDistances(Data data)
        {
            int length = data.ColNum * data.RowNum;
            byte[] encoded = new byte[length];
            for (int i = 0; i < length; ++i)
            {
                encoded[i] = LimitToByte(data.RawData[i], LimitedDistanceMinimum, LimitedDistanceMaximum);
            }
            return encoded;
        }

        static public void UpdateLimitedEncodedDistances(byte[] distances, Data data)
        {
            var rawData = data.RawData;
            int length = distances.Length;
            for (int i = 0; i < length; ++i)
            {
                rawData[i] = FromLimitedToByte(distances[i], LimitedDistanceMinimum, LimitedDistanceMaximum);
            }
        }

        private class BrushData
        {
            public enum Type { Erase, Add }
            private Type type;
            private float x;
            private float y;
            private float radius;

            public BrushData(Vector3 pos, float radius, Type type)
            {
                x = pos.x;
                y = pos.z;
                this.radius = radius;
                this.type = type;
            }

            public void Apply(Data data, float cellSize)
            {
                if (type == Type.Add)
                {
                    data.ApplyCircle(x, y, radius, cellSize);
                }
                else
                {
                    data.RemoveCircle(x, y, radius, cellSize);
                }
            }
        }

        private class UpdateData
        {
            private byte[] encodedData;

            public UpdateData(byte[] encodedData)
            {
                this.encodedData = encodedData;
            }

            public void Apply(Data data)
            {
                UpdateLimitedEncodedDistances(encodedData, data);
            }
        }

        public class ChunkData : IDisposable
        {
            public Data GroundData { get; private set; }
            public Data IceData { get; private set; }
            public Data BushData { get; private set; }
            public Data CliffData { get; private set; }

            public Data OriginalIceData { get; private set; }
            public Data OriginalBushData { get; private set; }

            public ChunkData(Data ground, Data ice, Data bush, Data cliff)
            {
                GroundData = ground;
                IceData = ice;
                BushData = bush;
                CliffData = cliff;

                OriginalIceData = Copy(IceData);
                OriginalBushData = Copy(BushData);
            }

            private Data Copy(Data original)
            {
                var data = new Data(original.ColNum, original.RowNum, null, original.HasHeights, null, original.HasCullingData, null);

                NativeArray<float>.Copy(original.RawData, data.RawData);

                if (original.HasHeights)
                {
                    NativeArray<float>.Copy(original.HeightsRawData, data.HeightsRawData);
                }

                if (original.HasCullingData)
                {
                    NativeArray<bool>.Copy(original.CullingDataRawData, data.CullingDataRawData);
                }

                return data;
            }

            public void Dispose()
            {
                GroundData?.Dispose();
                GroundData = null;

                IceData?.Dispose();
                IceData = null;

                BushData?.Dispose();
                BushData = null;

                CliffData?.Dispose();
                CliffData = null;

                OriginalIceData?.Dispose();
                OriginalIceData = null;

                OriginalBushData?.Dispose();
                OriginalBushData = null;
            }

            public void EraseBushAt(Vector3 pos, float radius, float cellSize)
                => BushData.RemoveCircle(pos.x, pos.z, radius, cellSize);

            public void EraseIceAt(Vector3 pos, float radius, float cellSize)
                => IceData.RemoveCircle(pos.x, pos.z, radius, cellSize);

            static public ChunkData LoadBinary(string path)
            {
                try
                {
                    using (BinaryReader reader = new BinaryReader(File.OpenRead(path)))
                    {
                        Data ground = ReadData(reader);
                        Data ice = ReadDataLimited(reader, LimitedDistanceMinimum, LimitedDistanceMaximum);
                        Data bush = ReadDataLimited(reader, LimitedDistanceMinimum, LimitedDistanceMaximum);
                        //Data ice = ReadData(reader);
                        //Data bush = ReadData(reader);
                        Data cliff = ReadData(reader);

                        return new ChunkData(ground, ice, bush, cliff);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
                return null;
            }

            static public void SaveBinary(string path, ChunkData data)
            {
                if (data == null)
                {
                    Debug.LogError("Can't save null data!");
                    return;
                }

                SaveBinary(path, data.GroundData, data.IceData, data.BushData, data.CliffData);
            }

            static public void SaveBinary(string path, Data ground, Data ice, Data bush, Data cliff)
            {
                try
                {
                    using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
                    {
                        WriteData(writer, ground);
                        WriteDataLimited(writer, ice, LimitedDistanceMinimum, LimitedDistanceMaximum);
                        WriteDataLimited(writer, bush, LimitedDistanceMinimum, LimitedDistanceMaximum);
                        //WriteData(writer, ice);
                        //WriteData(writer, bush);
                        WriteData(writer, cliff);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
        }

        [Serializable]
        private class LayerHandler
        {
            [SerializeField] private string layerName = "";
            [SerializeField] private string gameobjectLayer = "";
            [SerializeField] private MarchingSquaresInfo[] infos = null;
            [SerializeField] private float heightOffset = 0;
            [SerializeField] private MarchingSquaresComponent.InitializationInfo colliderMeshInfo = null;
            [SerializeField] private float colliderHeightOffset = 0;

            public float HeightOffset => heightOffset;

            private MarchingSquaresMesher[] meshers;
            private MeshDrawer[] drawers;

            private MarchingSquaresMesher colliderMesher;
            private Mesh colliderMesh;
            private MeshCollider meshCollider;

            private Transform Transform { get; set; }
            public int Layer { get; private set; }

            public bool IsGenerating { get; private set; }

            public void Init(Transform root, Data data)
            {
                Layer = LayerMask.NameToLayer(gameobjectLayer);

                var go = new GameObject(layerName);
                go.layer = Layer;

                Transform = go.transform;
                Transform.SetParent(root);
                Transform.localPosition = Vector3.up * colliderHeightOffset;
                
                colliderMesh = new Mesh();
                meshCollider = go.AddComponent<MeshCollider>();
                meshCollider.sharedMesh = colliderMesh;
                
                meshers = new MarchingSquaresMesher[infos.Length];
                drawers = new MeshDrawer[infos.Length];
                for (int i = 0; i < infos.Length; ++i)
                {
                    var info = infos[i];

                    meshers[i] = new MarchingSquaresMesher();
                    info.InitInfo.Init(meshers[i], data);
                    
                    drawers[i] = new MeshDrawer(info.RenderInfo);
                    drawers[i].Mesh = new Mesh();
                }

                colliderMesher = new MarchingSquaresMesher();
                colliderMeshInfo.Init(colliderMesher, data);
            }

            public void UpdateData(Data data)
            {
                for (int i = 0; i < infos.Length; ++i)
                {
                    var info = infos[i];
                    info.InitInfo.Init(meshers[i], data);
                }

                colliderMeshInfo.Init(colliderMesher, data);
            }

            public void StartGeneration()
            {
                IsGenerating = true;
                for (int i = 0; i < meshers.Length; ++i)
                {
                    if (!meshers[i].IsGenerating)
                    {
                        meshers[i].Start();
                    }
                }

                colliderMesher.Start();
            }

            public void EndGeneration()
            {
                if (IsGenerating)
                {
                    IsGenerating = false;
                    for (int i = 0; i < meshers.Length; ++i)
                    {
                        if (meshers[i].IsGenerating)
                        {
                            meshers[i].Complete(drawers[i].Mesh);
                        }
                    }

                    colliderMesher.Complete(colliderMesh);
                    meshCollider.sharedMesh = colliderMesh;
                }
            }

            public void Render(Camera cam)
            {
                for (int i = 0; i < drawers.Length; ++i)
                {
                    if (!drawers[i].Renderer.Hide)
                    {
                        var m = Transform.localToWorldMatrix;
                        m.m13 = heightOffset;
                        drawers[i].Render(cam, m, Layer);
                    }
                }
            }
        }
        
        [Serializable]
        private class MarchingSquaresInfo
        {
            [SerializeField] private MarchingSquaresComponent.InitializationInfo initInfo = null;
            public MarchingSquaresComponent.InitializationInfo InitInfo => initInfo;
            [SerializeField] private MeshDrawer.RenderInfo renderInfo = null;
            public MeshDrawer.RenderInfo RenderInfo => renderInfo;
        }
    }
}
