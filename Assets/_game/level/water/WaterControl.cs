﻿using UnityEngine;

public class WaterControl : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private float waterLevel = -1;

    private Material mat;
    private int surfaceNoiseId;
    private int surfaceDistortionId;

    private Quaternion originalRotation;

    void Start()
    {
        originalRotation = transform.rotation;

        mat = meshRenderer.sharedMaterial;
        surfaceNoiseId = Shader.PropertyToID("_SurfaceNoise");
        surfaceDistortionId = Shader.PropertyToID("_SurfaceDistortion");
    }

    void LateUpdate()
    {
        /*
        if (cam != null)
        {
            Vector3 s = transform.localScale;
            s.x = (cam.Right - cam.Left) / width;
            s.y = (cam.Top - cam.Bottom) / height;
            transform.localScale = s;

            transform.position = new Vector3(Mathf.Lerp(cam.Left, cam.Right, 0.5f), waterLevel, Mathf.Lerp(cam.Bottom, cam.Top, 0.5f));
        }
        */
        transform.rotation = originalRotation;

        Vector2 offset = new Vector2(0, 0);
        
        Vector2 scale = mat.GetTextureScale(surfaceNoiseId);
        offset.x = transform.position.x / (transform.localScale.x / scale.x);
        offset.y = transform.position.z / (transform.localScale.y / scale.y);
        mat.SetTextureOffset(surfaceNoiseId, offset);

        scale = mat.GetTextureScale(surfaceDistortionId);
        offset.x = transform.position.x / (transform.localScale.x / scale.x);
        offset.y = transform.position.z / (transform.localScale.y / scale.y); 
        mat.SetTextureOffset(surfaceDistortionId, offset);
        
        Vector3 pos = transform.position;
        pos.y = waterLevel;
        transform.position = pos;
        
    }
}
