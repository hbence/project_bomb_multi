﻿Shader "Toon/WaterShadow"
{
	Properties
	{
		_WaterColor("Water Color", Color) = (0.325, 0.807, 0.971, 0.725)
		_WaterShadowColor("Water Shadow Color", Color) = (0.325, 0.807, 0.971, 0.725)
		_FoamColor("Foam Color", Color) = (1,1,1,1)
		_SurfaceNoise("Surface Noise", 2D) = "white" {}
		_SurfaceNoiseScroll("Surface Noise Scroll Amount", Vector) = (0.03, 0.03, 0, 0)
		_SurfaceNoiseCutoff("Surface Noise Cutoff", Range(0, 1)) = 0.777
		_SurfaceDistortion("Surface Distortion", 2D) = "white" {}
		_SurfaceDistortionAmount("Surface Distortion Amount", Range(0, 1)) = 0.27
	}
		
	SubShader
	{
	Tags
	{
		"LightMode" = "ForwardBase"
	}

	Pass
	{
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off

		CGPROGRAM
#define SMOOTHSTEP_AA 0.04

#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"
#pragma multi_compile_fog
#define USING_FOG (defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2))

#pragma multi_compile_fwdbase

#include "AutoLight.cginc"
#include "UnityLightingCommon.cginc"

	half4 alphaBlend(half4 top, half4 bottom)
	{
		half3 color = (top.rgb * top.a) + (bottom.rgb * (1 - top.a));
		half alpha = top.a + bottom.a * (1 - top.a);

		return half4(color, alpha);
	}

	struct appdata
	{
		float4 vertex : POSITION;
		float4 uv : TEXCOORD0;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 pos : SV_POSITION;
		half4 diff : COLOR0;
		float2 noiseUV : TEXCOORD0;
		float2 distortUV : TEXCOORD1;

#if USING_FOG
		fixed fog : TEXCOORD2;
#endif
		LIGHTING_COORDS(3, 4)
	};

	sampler2D _SurfaceNoise;
	float4 _SurfaceNoise_ST;

	sampler2D _SurfaceDistortion;
	float4 _SurfaceDistortion_ST;

	v2f vert(appdata v)
	{
		v2f o;

		o.distortUV = TRANSFORM_TEX(v.uv, _SurfaceDistortion);
		o.noiseUV = TRANSFORM_TEX(v.uv, _SurfaceNoise);

		half waveSpeedScale = 0.03;
		half waveScale = 0.2;
		float2 noiseUV = float2(o.noiseUV.x + _Time.y * waveSpeedScale, o.noiseUV.y + _Time.y * waveSpeedScale);
		float noise = tex2Dlod(_SurfaceNoise, float4(noiseUV, 0.0, 0.0)).r;

		float4 p = v.vertex + float4(0.0, noise * waveScale, 0.0, 0.0);

		o.pos = UnityObjectToClipPos(p);

		half3 worldNormal = UnityObjectToWorldNormal(v.normal);
		half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
		o.diff = nl * _LightColor0;

		o.diff.rgb += ShadeSH9(half4(worldNormal, 1));

#if USING_FOG
		float3 eyePos = UnityObjectToViewPos(v.vertex);
		float fogCoord = length(eyePos.xyz);
		UNITY_CALC_FOG_FACTOR_RAW(fogCoord);
		o.fog = saturate(unityFogFactor);
#endif
		TRANSFER_VERTEX_TO_FRAGMENT(o);

		return o;
	}

	half4 _WaterColor;
	half4 _WaterShadowColor;
	half4 _FoamColor;

	float _SurfaceNoiseCutoff;
	float _SurfaceDistortionAmount;

	float2 _SurfaceNoiseScroll;

	float4 frag(v2f i) : SV_Target
	{
		float2 distortSample = (tex2D(_SurfaceDistortion, i.distortUV).xy * 2 - 1) * _SurfaceDistortionAmount;

		float2 noiseUV = float2((i.noiseUV.x + _Time.y * _SurfaceNoiseScroll.x) + distortSample.x,
			(i.noiseUV.y + _Time.y * _SurfaceNoiseScroll.y) + distortSample.y);
		float surfaceNoiseSample = tex2D(_SurfaceNoise, noiseUV).r;

		float surfaceNoise = smoothstep(_SurfaceNoiseCutoff - SMOOTHSTEP_AA, _SurfaceNoiseCutoff + SMOOTHSTEP_AA, surfaceNoiseSample);

		half4 surfaceNoiseColor = _FoamColor;
		surfaceNoiseColor.a *= surfaceNoise;

		half4 col = alphaBlend(surfaceNoiseColor, _WaterColor);
		col.rgb *= i.diff.rgb;

	#if USING_FOG
		col.rgb = lerp(unity_FogColor.rgb, col.rgb, i.fog);
	#endif

		float test = smoothstep(0.0, 1.0, 0.8 + surfaceNoiseSample);

		float attenuation = LIGHT_ATTENUATION(i);
		col.rgb = lerp(_WaterShadowColor.rgb, col.rgb, attenuation);
		col.rgb *= test;
		return col;
	}
		ENDCG
	}
	}
	Fallback "VertexLit"
}
