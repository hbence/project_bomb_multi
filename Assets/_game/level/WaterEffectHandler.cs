using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static BombGame.Utils;

namespace BombGame
{
    public class WaterEffectHandler : MonoBehaviour
    {
        [SerializeField] private EffectControl[] effects = null;

        private int nextEffect = 0;

        private void OnEnable()
        {
            foreach(var effect in effects)
            {
                Deactivate(effect);
            }
            nextEffect = 0;
        }

        private void OnTriggerEnter(Collider other)
        {
            AddEffectAt(other.transform.position);
        }

        private void AddEffectAt(Vector3 pos)
        {
            if (effects != null && effects.Length > 0)
            {
                var effect = effects[nextEffect];
                Deactivate(effect);
                Activate(effect);

                pos.y = transform.position.y;
                effect.transform.position = pos;

                nextEffect = (nextEffect + 1) % effects.Length;
            }
        }
    }
}