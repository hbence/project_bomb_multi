using UnityEngine;

using Mirror;

using static BombGame.Utils;

namespace BombGame
{
    public class LevelControl : NetworkBehaviour
    {
        [SerializeField] private Transform[] spawnPoints = null;
        [SerializeField] private LevelChunk[] chunks = null;

        [SerializeField] private EffectHandler bushDestroyEffects = null;
        [SerializeField] private EffectHandler iceDestroyEffects = null;

        public Transform[] SpawnPoints => spawnPoints;

        private int lastSpawnIndex = 0;
        public Vector3 NextSpawnPoint
        {
            get
            {
                lastSpawnIndex = (lastSpawnIndex + 1) % spawnPoints.Length;
                return spawnPoints[lastSpawnIndex].position;
            }
        }

        private void Awake()
        {
            if (chunks != null && chunks.Length > 0)
            {
                foreach (var chunk in chunks)
                {
                    chunk.Level = this;
                }
            }
        }

        public void EraseBushAt(Vector3 pos, float radius)
        {
            if (chunks != null)
            {
                foreach(var chunk in chunks)
                {
                    var rect = chunk.CalcFilledBushDataAt(pos, radius);
                    if (rect.width > 0 && rect.height > 0)
                    {
                        RpcAddBushEffect(rect);
                    }

                    chunk.EraseBushAt(pos, radius);
                }
            }
        }

        public void EraseIceAt(Vector3 pos, float radius)
        {
            if (chunks != null)
            {
                foreach (var chunk in chunks)
                {
                    var rect = chunk.CalcFilledIceDataAt(pos, radius);
                    if (rect.width > 0 && rect.height > 0)
                    {
                        RpcAddIceEffect(rect);
                    }

                    chunk.EraseIceAt(pos, radius);
                }
            }
        }

        public void UpdatedBushLayerOnServer(LevelChunk chunk, byte[] data)
        {
            for (int i = 0; i < chunks.Length; ++i)
            {
                if (chunks[i] == chunk)
                {
                    RpcUpdateBushData(i, data);
                    break;
                }
            }
        }

        [Command(requiresAuthority = false)]
        public void CmdRequestUpdates(NetworkConnectionToClient sender = null)
        {
            if (chunks != null && chunks.Length > 0)
            {
                for (int i = 0; i < chunks.Length; ++i)
                {
                    var chunk = chunks[i];
                    var bushData = LevelChunk.EncodeLimitedDataDistances(chunk.BushData);
                    var iceData = LevelChunk.EncodeLimitedDataDistances(chunk.IceData);
                    TargetUpdate(sender, i, bushData, iceData);
                }
            }
        }

        [TargetRpc]
        private void TargetUpdate(NetworkConnection target, int chunkIndex, byte[] bushData, byte[] iceData)
        {
            chunks[chunkIndex].UpdateBush(bushData);
            chunks[chunkIndex].UpdateIce(iceData);
        }

        [ClientRpc]
        private void RpcUpdateBushData(int chunkIndex, byte[] data)
        {
            if (isClientOnly)
            {
                chunks[chunkIndex].UpdateBush(data);
            }
        }

        public void UpdatedIceLayerOnServer(LevelChunk chunk, byte[] data)
        {
            for (int i = 0; i < chunks.Length; ++i)
            {
                if (chunks[i] == chunk)
                {
                    RPCUpdateIceData(i, data);
                    break;
                }
            }
        }

        [ClientRpc]
        private void RPCUpdateIceData(int chunkIndex, byte[] data)
        {
            if (isClientOnly)
            {
                chunks[chunkIndex].UpdateIce(data);
            }
        }

        [ClientRpc]
        private void RpcAddBushEffect(Rect rect)
        {
            bushDestroyEffects.AddEffectAt(rect);
        }

        [ClientRpc]
        private void RpcAddIceEffect(Rect rect)
        {
            iceDestroyEffects.AddEffectAt(rect);
        }

        [System.Serializable]
        private class EffectHandler
        {
            [SerializeField] private AreaParticleEffects[] effects;
            [SerializeField] private float effectShowY = 0;

            private int nextIndex = 0;

            public void DisableAll()
            {
                if (effects != null && effects.Length > 0)
                {
                    foreach(var effect in effects)
                    {
                        Deactivate(effect);
                    }
                }
                nextIndex = 0;
            }

            public void AddEffectAt(Rect rect)
            {
                if (effects != null && effects.Length > 0)
                {
                    var effect = effects[nextIndex];

                    Deactivate(effect);
                    Activate(effect);

                    Vector3 pos = new Vector3(rect.center.x, effectShowY, rect.center.y);
                    effect.transform.position = pos;

                    effect.SetArea(rect);

                    nextIndex = (nextIndex + 1) % effects.Length;
                }
            }
        }
    }
}