﻿using System.Collections.Generic;
using UnityEngine;

namespace BombGame
{
    public class ElemCache<EnumType, ObjectType> where ObjectType : MonoBehaviour, CachedObject<EnumType>
    {
        private Transform root;
        private Theme<EnumType> theme;
        private Dictionary<EnumType, List<ObjectType>> cache;

        public ElemCache(Transform cacheRoot, Theme<EnumType> theme)
        {
            this.root = cacheRoot;
            this.theme = theme;
            cache = new Dictionary<EnumType, List<ObjectType>>();
        }

        public void Cache(EnumType type, int count)
        {
            List<ObjectType> list = null;
            cache.TryGetValue(type, out list);
            if (list == null)
            {
                list = new List<ObjectType>();
                cache[type] = list;
            }

            for (int i = 0; i < count; ++i)
            {
                // get prefab in loop in case there are multiple prefabs for the same type
                GameObject prefab = theme.GetPrefab(type);
                GameObject go = GameObject.Instantiate<GameObject>(prefab);
                if (go != null)
                {
                    go.SetActive(false);
                    go.transform.SetParent(root);

                    var obj = go.GetComponent<ObjectType>();
                    if (obj != null)
                    {
                        list.Add(obj);
                    }
                    else
                    {
                        Debug.LogError("object has no ObjectType component for type:" + type);
                    }
                }
                else
                {
                    Debug.LogError("New object of type:" + type + " is null");
                }
            }
        }

        public ObjectType Get(EnumType type)
        {
            List<ObjectType> list = null;
            cache.TryGetValue(type, out list);
            if (list == null)
            {
                Debug.Log("No list for type:" + type + " creating new");
                Cache(type, 1);
                cache.TryGetValue(type, out list);
            }

            if (list.Count == 0)
            {
                Debug.Log("Couldn't find inactive go of type:" + type + " creating new");
                Cache(type, 1);
            }

            var obj = list[list.Count - 1];
            list.RemoveAt(list.Count - 1);

            obj.gameObject.SetActive(true);
            obj.transform.SetParent(null);

            return obj;
        }

        public void Free(ObjectType obj)
        {
            obj.gameObject.SetActive(false);

            List<ObjectType> list = null;
            cache.TryGetValue(obj.Type, out list);
            if (list != null)
            {
                list.Add(obj);
                obj.transform.SetParent(root);
            }
            else
            {
                Debug.Log("Tried to free none cached elem object! it's destroyed");
                GameObject.DestroyImmediate(obj.gameObject);
            }
        }
    }
}