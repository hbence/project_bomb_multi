﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BombGame
{
    public class Utils
    {
        static public void Activate(GameObject go) => go?.SetActive(true);
        static public void Deactivate(GameObject go) => go?.SetActive(false);

        static public void Activate(MonoBehaviour c) => c?.gameObject.SetActive(true);
        static public void Deactivate(MonoBehaviour c) => c?.gameObject.SetActive(false);

        static public void Activate(Transform c) => c?.gameObject.SetActive(true);
        static public void Deactivate(Transform c) => c?.gameObject.SetActive(false);

        static public bool IsActive(GameObject go) => go != null ? go.activeSelf : false;

        static public bool IsActive(MonoBehaviour c) => c != null ? c.gameObject.activeSelf : false;

        static public bool IsActive(Transform c) => c != null ? c.gameObject.activeSelf : false;

        static public void SetActive(GameObject go, bool value) => go?.SetActive(value);

        static public void SetActive(MonoBehaviour c, bool value) => c?.gameObject.SetActive(value);

        static public void SetActive(Transform c, bool value) => c?.gameObject.SetActive(value);

        static public bool IsObjectLayer(GameObject go, int layer) => go.layer == layer;
        static public bool IsObjectLayer(Transform t, int layer) => t.gameObject.layer == layer;
        static public bool IsObjectLayer(Collision c, int layer) => c.gameObject.layer == layer;

        [System.Serializable]
        public class Timer
        {
            [SerializeField] private float duration = 0;
            public float Duration { get => duration; set => duration = value; }
            public float Value { get; private set; } = 0;

            public Timer() { }
            public Timer(float duration) { Duration = duration; }

            public void Step(float dt) { Value = Mathf.Clamp(Value + dt, 0, duration); }
            public void StepUnclamped(float dt) { Value += dt; }

            public void Reset() { Value = 0; }

            public bool IsOver { get => Value >= Duration; }
            public float Progress { get => Value / Duration; }
        }

        public static bool IsPointerOverGameObject
        {
            get
            {
                if (EventSystem.current == null)
                {
                    return false;
                }

                //check mouse
                if (EventSystem.current.IsPointerOverGameObject())
                    return true;

                //check touch
                if (Input.touchCount > 0 && Input.touches != null && Input.touches[0].phase == TouchPhase.Began)
                {
                    if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                        return true;
                }

                if (IsPointerOverUIObject)
                {
                    return true;
                }

                return false;
            }
        }

        private static bool IsPointerOverUIObject
        {
            get
            {
                PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
                eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                var results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
                return results.Count > 0;
            }
        }

        public static T FirstElem<T>(T[] array) { return array[0]; }
        public static T FirstElem<T>(List<T> list) { return list[0]; }
        public static T LastElem<T>(T[] array) { return array[array.Length - 1]; }
        public static T LastElem<T>(List<T> list) { return list[list.Count - 1]; }

        [System.Serializable]
        public class Range
        {
            [SerializeField] private float min;
            [SerializeField] private float max;
            public float Min { get => min; set => min = value; }
            public float Max { get => max; set => max = value; }
            public Range(float value) { Min = value; Max = value; }
            public Range(float min, float max) { Min = min; Max = max; }
            public float RandomInside { get => Mathf.Lerp(min, max, Random.value); }
            public float Distance { get => max - min; }

            public Range Copy() => (Range)MemberwiseClone();
        }

        public static int CalcLayerMask(params string[] args)
        {
            int res = 0;
            foreach (var layer in args)
            {
                res |= 1 << LayerMask.NameToLayer(layer);
            }
            return res;
        }

#if UNITY_EDITOR

        static public void DrawString(string text, Vector3 worldPos, float oX = 0, float oY = 0, Color? colour = null)
        {
            UnityEditor.Handles.BeginGUI();

            var restoreColor = GUI.color;

            if (colour.HasValue) GUI.color = colour.Value;
            var view = UnityEditor.SceneView.currentDrawingSceneView;
            Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);

            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreColor;
                UnityEditor.Handles.EndGUI();
                return;
            }

            UnityEditor.Handles.Label(TransformByPixel(worldPos, oX, oY), text);

            GUI.color = restoreColor;
            UnityEditor.Handles.EndGUI();
        }

        static Vector3 TransformByPixel(Vector3 position, float x, float y)
        {
            return TransformByPixel(position, new Vector3(x, y));
        }

        static Vector3 TransformByPixel(Vector3 position, Vector3 translateBy)
        {
            Camera cam = UnityEditor.SceneView.currentDrawingSceneView.camera;
            if (cam)
                return cam.ScreenToWorldPoint(cam.WorldToScreenPoint(position) + translateBy);
            else
                return position;
        }

#else
    
    static public void DrawString(string text, Vector3 worldPos, float oX = 0, float oY = 0, Color? colour = null)
    {
    }

#endif

    }
}