﻿
namespace BombGame
{
    public interface CachedObject<EnumType>
    {
        EnumType Type { get; }
    }
}
