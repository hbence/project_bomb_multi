﻿using UnityEngine;

namespace BombGame
{
    public class Easing
    {
        public enum Type
        {
            Linear = 1 << 0,
            InQuadratic = 1 << 1,
            OutQuadratic = 1 << 2,
            InCubic = 1 << 3,
            OutCubic = 1 << 4,
            InElastic = 1 << 5,
            OutElastic = 1 << 6,
            InBounce = 1 << 7,
            OutBounce = 1 << 8,

            InOutQuadratic = InQuadratic | OutQuadratic,
            InOutCubic = InCubic | OutCubic
        }

        [System.Serializable]
        public class Info
        {
            public float begin;
            public float end;
            public float duration;
            public Type type;

            public Object CreateObject()
            {
                return new Object(this);
            }
        }

        public class Object
        {
            public float Begin;
            public float End;

            public float Time;

            public float Duration;

            private Type type;
            public Type Type
            {
                get => type;
                private set
                {
                    type = value;
                    easer = SelectEaser(type);
                }
            }

            public float Value { get; private set; }

            private delegate float EaserFn(float begin, float end, float time, float duration);
            private EaserFn easer;

            public Object(float begin, float end, float duration, Type type)
            {
                Set(begin, end, duration, type);
            }

            public Object(Info info)
            {
                Set(info);
            }

            public void Set(float begin, float end, float duration, Type type)
            {
                Begin = begin;
                End = end;
                Duration = duration;
                Type = type;
                Reset();
            }

            public void Set(Info info)
            {
                Set(info.begin, info.end, info.duration, info.type);
            }

            public void Reset()
            {
                forward = true;
                playedForward = false;
                Time = 0;
                Value = Begin;
            }

            public void Step(float delta)
            {
                if (Time < Duration)
                {
                    Time = Mathf.Clamp(Time + delta, 0, Duration);
                    Value = easer(Begin, End, Time, Duration);
                }
                else
                {
                    Value = End;
                }
            }

            public void StepBack(float delta)
            {
                if (Time > 0)
                {
                    Time = Mathf.Clamp(Time - delta, 0, Duration);
                    Value = easer(Begin, End, Time, Duration);
                }
                else
                {
                    Value = Begin;
                }
            }

            private bool forward = true;
            public void StepBackAndForth(float delta)
            {
                if (forward)
                {
                    Step(delta);
                    if (IsAtEnd) { forward = false; }
                }
                else
                {
                    StepBack(delta);
                    if (IsAtStart) { forward = true; }
                }
            }

            private bool playedForward = false;
            public void StepBackAndForthOnce(float delta)
            {
                if (!HasPlayedOnce)
                {
                    StepBackAndForth(delta);

                    if (!forward)
                    {
                        playedForward = true;
                    }
                    if (HasPlayedOnce)
                    {
                        Time = 0;
                    }
                }
            }

            public float Between(float a, float b) { return Mathf.LerpUnclamped(a, b, Value); }

            public Vector3 Between(Vector3 a, Vector3 b) { return Vector3.LerpUnclamped(a, b, Value); }

            public Color Between(Color a, Color b) { return Color.Lerp(a, b, Value); }

            public void Finish() { Time = Duration; Value = End; }

            public bool IsAtEnd { get => Time >= Duration; }
            public bool IsAtStart { get => Time <= 0; }

            public bool HasPlayedOnce { get => playedForward && forward; }
            public void FinishBackAndForthOnce() { Time = 0; forward = true; playedForward = true; }

            private static EaserFn SelectEaser(Type type)
            {
                EaserFn easerFn = Linear;

                switch (type)
                {
                    case Type.Linear: easerFn = Linear; break;
                    case Type.InQuadratic: easerFn = InQuadratic; break;
                    case Type.OutQuadratic: easerFn = OutQuadratic; break;
                    case Type.InCubic: easerFn = InCubic; break;
                    case Type.OutCubic: easerFn = OutCubic; break;
                    case Type.InElastic: easerFn = InElastic; break;
                    case Type.OutElastic: easerFn = OutElastic; break;
                    case Type.InBounce: easerFn = InBounce; break;
                    case Type.OutBounce: easerFn = OutBounce; break;
                    case Type.InOutQuadratic: easerFn = InOutQuadratic; break;
                    case Type.InOutCubic: easerFn = InOutCubic; break;
                }

                // the easer function use different parameters
                return (float begin, float end, float time, float duration) =>
                {
                    float change = end - begin;
                    return easerFn(time, begin, change, duration);
                };
            }
        }

        public static float Ease(float begin, float end, float time, float duration, Type type)
        {
            float change = end - begin;

            switch (type)
            {
                case Type.Linear: return Linear(time, begin, change, duration);
                case Type.InQuadratic: return InQuadratic(time, begin, change, duration);
                case Type.OutQuadratic: return OutQuadratic(time, begin, change, duration);
                case Type.InCubic: return InCubic(time, begin, change, duration);
                case Type.OutCubic: return OutCubic(time, begin, change, duration);
                case Type.InElastic: return InElastic(time, begin, change, duration);
                case Type.OutElastic: return OutElastic(time, begin, change, duration);
                case Type.InBounce: return InBounce(time, begin, change, duration);
                case Type.OutBounce: return OutBounce(time, begin, change, duration);
                case Type.InOutQuadratic: return InOutQuadratic(time, begin, change, duration);
                case Type.InOutCubic: return InOutCubic(time, begin, change, duration);
            }

            return Linear(time, begin, change, duration);
        }

        public static Vector2 Ease(Vector2 begin, Vector2 end, float time, float duration, Type type)
        {
            float t = Ease(0, 1, time, duration, type);
            return Vector2.Lerp(begin, end, t);
        }

        public static Vector3 Ease(Vector3 begin, Vector3 end, float time, float duration, Type type)
        {
            float t = Ease(0, 1, time, duration, type);
            return Vector3.Lerp(begin, end, t);
        }

        public static float Linear(float t, float b, float c, float d)
        {
            return c * t / d + b;
        }

        public static float InQuadratic(float t, float b, float c, float d)
        {
            t /= d;
            return c * t * t + b;
        }

        public static float OutQuadratic(float t, float b, float c, float d)
        {
            t /= d;
            return -c * t * (t - 2) + b;
        }

        public static float InOutQuadratic(float t, float b, float c, float d)
        {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            --t;
            return -c / 2 * (t * (t - 2) - 1) + b;
        }

        public static float InCubic(float t, float b, float c, float d)
        {
            t /= d;
            return c * t * t * t + b;
        }

        public static float OutCubic(float t, float b, float c, float d)
        {
            t /= d;
            t--;
            return c * (t * t * t + 1) + b;
        }

        public static float InOutCubic(float t, float b, float c, float d)
        {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t * t + b;
            t -= 2;
            return c / 2 * (t * t * t + 2) + b;
        }

        public static float InElastic(float t, float b, float c, float d)
        {
            t /= d;
            float ts = t * t;
            float tc = ts * t;
            return b + c * (33 * tc * ts + -59 * ts * ts + 32 * tc + -5 * ts);
        }

        public static float OutElastic(float t, float b, float c, float d)
        {
            t /= d;
            float ts = t * t;
            float tc = ts * t;
            return b + c * (33 * tc * ts + -106 * ts * ts + 126 * tc + -67 * ts + 15 * t);
        }

        public static float OutBounce(float t, float b, float c, float d)
        {
            t = t / d;
            if (t < 1.0f / 2.75f)
            {
                return c * (7.56250f * t * t) + b;
            }
            else if (t < 2.0f / 2.75f)
            {
                t = t - (1.5f / 2.75f);
                return c * (7.5625f * t * t + 0.75f) + b;
            }
            else if (t < 2.5f / 2.75f)
            {
                t = t - (2.25f / 2.75f);
                return c * (7.5625f * t * t + 0.9375f) + b;
            }
            else
            {
                t = t - (2.625f / 2.75f);
                return c * (7.5625f * t * t + 0.984375f) + b;
            }
        }

        public static float InBounce(float t, float b, float c, float d)
        {
            return c - OutBounce(d - t, 0, c, d) + b;
        }

        [System.Serializable]
        public class IntervalMap
        {
            public float sourceMin;
            public float sourceMax;

            public float targetMin;
            public float targetMax;

            public Type easing;

            public float Get(float value)
            {
                float t = Mathf.Clamp01((value - sourceMin) / (sourceMax - sourceMin));
                return Ease(targetMin, targetMax, t, 1f, easing);
            }
        }
    }
}
