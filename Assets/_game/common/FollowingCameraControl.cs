using System;
using UnityEngine;

using static BombGame.Utils;

namespace BombGame
{
    public class FollowingCameraControl : MonoBehaviour
    {
        private static readonly Vector3 Up = Vector3.up;
        private static readonly Quaternion NoRotation = Quaternion.identity;

        [SerializeField] private Transform root = null;
        [SerializeField] private Camera cam = null;
        public Camera Camera => cam;
        [SerializeField] private Transform target = null;
        public Transform Target
        {
            get => target;
            set => target = value;
        }

        [SerializeField] private CameraSettings settings = null;
        public CameraSettings.TargetFollowMode TargetFollowMode { get => settings.FollowMode; set => settings.FollowMode = value; }
        public float ViewCenterX { get => settings.ViewCenterX; set => settings.ViewCenterX = value; }
        public float ViewCenterY { get => settings.ViewCenterY; set => settings.ViewCenterY = value; }

        public float VerticalAngle { get => settings.VerticalAngle; set { settings.VerticalAngle = value; updateCamera = true; } }
        public float HorizontalAngle { get => settings.HorizontalAngle; set { settings.HorizontalAngle = value; updateCamera = true; } }

        [SerializeField] private Easing.Info settingsEaserInfo = null;

        private bool updateCamera = false;

        private Easing.Object settingsEaser;
        private CameraSettings prevSettings;

        private Easing.Object floatingEaser;
        private Vector3 lastCameraRotation;
        private Vector3 targetCameraRotation;

        private void Awake()
        {
            floatingEaser = new Easing.Object(0, 1, 1, Easing.Type.InOutQuadratic);
            floatingEaser.Finish();

            settingsEaser = settingsEaserInfo.CreateObject();
        }

        private void OnEnable()
        {
            lastCameraRotation = Vector3.zero;
            targetCameraRotation = Vector3.zero;
            floatingEaser.Finish();

            prevSettings = null;
            floatingEaser.Finish();
            PlaceCamera(settings);

            updateCamera = true;
        }

        void Update()
        {
            if (updateCamera)
            {
                updateCamera = false;
                PlaceCamera(settings);
            }

            if (target != null)
            {
                if (prevSettings != null && !settingsEaser.IsAtEnd)
                {
                    settingsEaser.Step(Time.deltaTime);
                    EaseCamera(prevSettings, settings, settingsEaser.Value);

                    root.position += settingsEaser.Between(CalcFollowDeltaStep(prevSettings, Time.deltaTime),
                                                            CalcFollowDeltaStep(settings, Time.deltaTime));

                    if (settingsEaser.IsAtEnd)
                    {
                        prevSettings = null;
                    }
                }
                else
                {
                    if (settings.FollowMode != CameraSettings.TargetFollowMode.None)
                    {
                        root.position += CalcFollowDeltaStep(settings, Time.deltaTime);
                    }
                }
            }

            if (settings.IsFloating)
            {
                HandleCameraFloating(settings.Floating, Time.deltaTime);
            }
        }

        private void PlaceCamera(float hzAngle, float vcAngle, float distance)
        {
            cam.transform.localPosition = new Vector3(0, 0, -Mathf.Abs(distance));
            cam.transform.localRotation = Quaternion.Lerp(cam.transform.localRotation, NoRotation, Time.deltaTime);

            root.rotation = Quaternion.Euler(vcAngle, hzAngle, 0);
        }

        private void PlaceCamera(CameraSettings camSettings)
            => PlaceCamera(camSettings.HorizontalAngle, camSettings.VerticalAngle, camSettings.Distance);

        private void EaseCamera(CameraSettings prevSettings, CameraSettings targetSettings, float t)
        {
            float prevHzAngle = prevSettings.HorizontalAngle;
            float targetHzAngle = targetSettings.HorizontalAngle;

            targetHzAngle = targetHzAngle < 0 ? targetHzAngle + 360f : targetHzAngle;

            if (Distance(targetHzAngle, prevHzAngle + 360) < Distance(targetHzAngle, prevHzAngle))
            {
                prevHzAngle += 360f;
            }
            else if (Distance(targetHzAngle, prevHzAngle - 360) < Distance(targetHzAngle, prevHzAngle))
            {
                prevHzAngle -= 360f;
            }

            PlaceCamera(
                Mathf.Lerp(prevHzAngle, targetHzAngle, t),
                Mathf.Lerp(prevSettings.VerticalAngle, targetSettings.VerticalAngle, t),
                Mathf.Lerp(prevSettings.Distance, targetSettings.Distance, t)
            );
        }

        private static float Distance(float a, float b) => Mathf.Abs(a - b);

        private Vector3 CalcFollowTargetDelta(float viewX, float viewY, float t)
        {
            Vector3 hitPoint;
            if (GetWorldPosAtViewPoint(viewX, viewY, out hitPoint))
            {
                var delta = target.position - hitPoint;
                return new Vector3(delta.x * t, 0, delta.z * t);
            }
            return Vector3.zero;
        }

        private Vector3 CalcFollowInFrameDelta(float hzBorder, float vcBorder, float t)
        {
            Vector3 targetPos = target.position;
            Vector3 bottomLeft, topRight;
            GetWorldPosAtViewPoint(hzBorder, vcBorder, out bottomLeft);
            GetWorldPosAtViewPoint(1f - hzBorder, 1f - vcBorder, out topRight);
            float deltaX = 0;
            float deltaZ = 0;
            if (targetPos.x < bottomLeft.x)
            {
                deltaX = targetPos.x - bottomLeft.x;
            }
            else if (targetPos.x > topRight.x)
            {
                deltaX = targetPos.x - topRight.x;
            }
            if (targetPos.z < bottomLeft.z)
            {
                deltaZ = targetPos.z - bottomLeft.z;
            }
            else if (targetPos.z > topRight.z)
            {
                deltaZ = targetPos.z - topRight.z;
            }
            return new Vector3(deltaX * t, 0, deltaZ * t);
        }

        private bool GetWorldPosAtViewPoint(float viewX, float viewY, out Vector3 hitPos)
        {
            float y = target.transform.position.y;
            Plane targetPlane = new Plane(Up, y);
            var ray = cam.ViewportPointToRay(new Vector3(viewX, viewY, 0));
            float enter;
            if (targetPlane.Raycast(ray, out enter))
            {
                hitPos = ray.GetPoint(enter);
                return true;
            }

            hitPos = Vector3.zero;
            return false;
        }

        private Vector3 CalcFollowTargetDelta(CameraSettings camSettings, float t)
            => CalcFollowTargetDelta(camSettings.ViewCenterX, camSettings.ViewCenterY, t);

        private Vector3 CalcFollowInFrameDelta(CameraSettings camSettings, float t)
            => CalcFollowInFrameDelta(camSettings.HzBorder, camSettings.VcBorder, t);

        private Vector3 CalcFollowDeltaStep(CameraSettings camSettings, float dt)
            => camSettings.FollowMode == CameraSettings.TargetFollowMode.Center ? CalcFollowTargetDelta(camSettings, dt * camSettings.FollowLerpMultiplier) :
               camSettings.FollowMode == CameraSettings.TargetFollowMode.InFrame ? CalcFollowInFrameDelta(camSettings, dt * camSettings.FollowLerpMultiplier) :
                Vector3.zero;

        private Vector3 CalcFollowDeltaToTarget(CameraSettings camSettings)
            => camSettings.FollowMode == CameraSettings.TargetFollowMode.Center ? CalcFollowTargetDelta(camSettings, 1) :
               camSettings.FollowMode == CameraSettings.TargetFollowMode.InFrame ? CalcFollowInFrameDelta(camSettings, 1) :
                Vector3.zero;

        public void SnapTo(CameraSettings cameraSettings)
        {
            settings = cameraSettings;
            PlaceCamera(settings);

            prevSettings = null;
            settingsEaser.Finish();
        }

        public void SnapToTarget()
        {
            root.position += CalcFollowDeltaToTarget(settings);
        }

        public void EaseTo(CameraSettings cameraSettings)
        {
            if (settings == cameraSettings)
            {
                return;
            }

            if (prevSettings != null && !settingsEaser.IsAtEnd)
            {
                prevSettings = cameraSettings.Copy();
                prevSettings.Distance = -cam.transform.localPosition.z;
                var euler = transform.eulerAngles;
                prevSettings.HorizontalAngle = euler.y;
                prevSettings.VerticalAngle = euler.x;
            }
            else
            {
                prevSettings = settings;
            }

            settings = cameraSettings;
            settingsEaser.Reset();

            lastCameraRotation = cam.transform.localEulerAngles;
            targetCameraRotation = lastCameraRotation;
            floatingEaser.Finish();
        }

        private void HandleCameraFloating(FloatingSettings floating, float deltaTime)
        {
            if (floatingEaser.IsAtEnd)
            {
                floatingEaser.Duration = floating.RandomToTargetDuration;
                floatingEaser.Reset();

                lastCameraRotation = targetCameraRotation;
                targetCameraRotation = new Vector3(floating.RandomXRotation, floating.RandomYRotation, floating.RandomZRotation);
            }

            floatingEaser.Step(deltaTime);
            cam.transform.localEulerAngles = floatingEaser.Between(lastCameraRotation, targetCameraRotation);
        }

        [Serializable]
        public class CameraSettings
        {
            [SerializeField] private float distance;
            public float Distance { get => distance; set => distance = value; }
            [SerializeField] private float horizontalAngle;
            public float HorizontalAngle { get => horizontalAngle; set => horizontalAngle = value; }

            [SerializeField] private float verticalAngle;
            public float VerticalAngle { get => verticalAngle; set => verticalAngle = value; }

            [Header("target")]
            [SerializeField] private TargetFollowMode followMode = TargetFollowMode.Center;
            public TargetFollowMode FollowMode { get => followMode; set => followMode = value; }

            [SerializeField] private float followLerpMultiplier = 1f;
            public float FollowLerpMultiplier => followLerpMultiplier;

            [SerializeField] private float viewCenterX = 0.5f;
            public float ViewCenterX { get => viewCenterX; set => viewCenterX = value; }

            [SerializeField] private float viewCenterY = 0.5f;
            public float ViewCenterY { get => viewCenterY; set => viewCenterY = value; }

            [SerializeField] private float hzBorder = 0.1f;
            public float HzBorder => hzBorder;
            [SerializeField] private float vcBorder = 0.1f;
            public float VcBorder => vcBorder;

            [Header("floating")]
            [SerializeField] private bool isFloating = false;
            public bool IsFloating => isFloating;
            [SerializeField] private FloatingSettings floating;
            public FloatingSettings Floating => floating;

            public enum TargetFollowMode
            {
                None,
                Center,
                InFrame
            }

            public CameraSettings Copy()
            {
                CameraSettings copy = (CameraSettings)MemberwiseClone();
                copy.floating = floating.Copy();
                return copy;
            }
        }

        [Serializable]
        public class FloatingSettings
        {
            [SerializeField] private Range xRotation = new Range(-0.05f, 0.05f);
            public float XRotationMin => xRotation.Min;
            public float XRotationMax => xRotation.Max;
            public float RandomXRotation => xRotation.RandomInside;

            [SerializeField] private Range yRotation = new Range(-0.05f, 0.05f);
            public float YRotationMin => yRotation.Min;
            public float YRotationMax => yRotation.Max;
            public float RandomYRotation => yRotation.RandomInside;

            [SerializeField] private Range zRotation = new Range(-0.05f, 0.05f);
            public float ZRotationMin => zRotation.Min;
            public float ZRotationMax => zRotation.Max;
            public float RandomZRotation => zRotation.RandomInside;

            [SerializeField] private Range toTargetDuration = new Range(1f, 3f);
            public float ToTargetDurationMin => toTargetDuration.Min;
            public float ToTargetDurationMax => toTargetDuration.Max;
            public float RandomToTargetDuration => toTargetDuration.RandomInside;

            public FloatingSettings Copy()
                => new FloatingSettings
                {
                    xRotation = xRotation.Copy(),
                    yRotation = yRotation.Copy(),
                    zRotation = zRotation.Copy(),
                    toTargetDuration = toTargetDuration.Copy()
                };
        }
    }
}
