﻿using UnityEngine;

namespace BombGame
{
    public interface Theme<EnumType>
    {
        GameObject GetPrefab(EnumType type);
    }
}
