﻿using UnityEngine;

namespace BombGame
{
    [CreateAssetMenu(fileName = "elem_theme", menuName = "Game/Create Game Elem Theme", order = 0)]
    public class ElemTheme : ScriptableObject, Theme<ElemTheme.Type>
    {
        public enum Type
        {
            None = 0,

            PlayerAvatar = 1,

            NormalBomb = 10,

            Explosion = 30
        }

        [SerializeField] private GameObject playerAvatar = null;
        [SerializeField] private GameObject normalBomb = null;
        [SerializeField] private GameObject explosion = null;

        public GameObject GetPrefab(Type type)
        {
            switch (type)
            {
                case Type.PlayerAvatar: return playerAvatar;
                case Type.NormalBomb: return normalBomb;
                case Type.Explosion: return explosion;
            }

            Debug.LogError($"type not handled! ({type})");

            return null;
        }
    }
}
