﻿using UnityEngine;

namespace BombGame
{
    public class CachedElem : MonoBehaviour, CachedObject<ElemTheme.Type>
    {
        [SerializeField] private ElemTheme.Type type = ElemTheme.Type.None;
        public ElemTheme.Type Type => type;

        public GameControl Game { get; set; }
    }
}
