using UnityEngine;
using Mirror;

namespace BombGame
{
    public class ClientInstance : NetworkBehaviour
    {
        public GameControl Game { get; set; }

        [SyncVar]
        public AvatarCharacterController avatar;
        public AvatarCharacterController Avatar => avatar;

        private Controls controls;

        public override void OnStartServer()
        {
            base.OnStartServer();

            NetworkSpawnPlayer();
        }

        public override void OnStartAuthority()
        {
            base.OnStartAuthority();

            if (controls == null)
            {
                controls = new Controls();
                controls.Player.Respawn.performed += (_) =>
                {
                    if (Avatar != null && Avatar.IsDead) 
                    {
                        CmdRespawnPlayer(); 
                    }
                };
            }

            controls.Enable();
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
            {
                Game.Level.CmdRequestUpdates();
            }

            base.OnStartClient();
        }

        public override void OnStopAuthority()
        {
            base.OnStopAuthority();

            controls?.Disable();
        }

        [Server]
        public void NetworkSpawnPlayer()
        {
            GameObject go = Game.SpawnAvatar(transform.position);
            go.name = $"avatar | {gameObject.name}";
            NetworkServer.Spawn(go, connectionToClient);

            avatar = go.GetComponent<AvatarCharacterController>();

            if (isLocalPlayer)
            {
                Game.AddCameraToPlayer(Avatar);
            }

            Debug.Log("NetworkSpawnPlayer()");
        }

        [Command]
        private void CmdRespawnPlayer()
        {
            var start = GameNetworkManager.Instance.GetStartPosition();
            Game.RespawnAvatar(start.position, Avatar);

            RpcRespawnPlayer(start.position);
        }

        [ClientRpc]
        private void RpcRespawnPlayer(Vector3 startPos)
        {
            Game.RespawnAvatar(startPos, Avatar);
        }
    }
}