﻿using UnityEngine;

namespace BombGame
{
    [System.Serializable]
    public class AudioClipHandler
    {
        public AudioClip clip;
        public float minPitch = 1f;
        public float maxPitch = 1f;
        public float volumeScale = 1f;

        public void Play()
        {
            if (clip != null)
            {
                AudioPlayer.PlaySound(clip, volumeScale, Random.Range(minPitch, maxPitch));
            }
        }

        public void Play(float scale = 1f)
        {
            if (clip != null)
            {
                AudioPlayer.PlaySound(clip, volumeScale * scale, Random.Range(minPitch, maxPitch));
            }
        }

        public void Play(AudioSource source, float scale = 1f)
        {
            if (AudioPlayer.SoundVolume > 0f && volumeScale > 0f)
            {
                source.clip = clip;
                source.pitch = Random.Range(minPitch, maxPitch);
                source.volume = AudioPlayer.SoundVolume * volumeScale * scale;
                source.PlayOneShot(clip);
            }
        }
    }
}
