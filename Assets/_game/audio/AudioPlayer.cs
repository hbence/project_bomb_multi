﻿using UnityEngine;

namespace BombGame
{
    public class AudioPlayer : MonoBehaviour
    {
        static public AudioPlayer Instance { get; private set; }

        [SerializeField] private AudioClipHandler clickSound = null;
        [SerializeField] private AudioClip defaultMusic = null;

        private AudioSource musicSource;
        private AudioSource[] soundSources;
        private int soundSourceIndex = 0;

        [SerializeField] private float musicVolume = 1f;
        [SerializeField] private float soundVolume = 1f;

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;

                musicSource = gameObject.AddComponent<AudioSource>();
                musicSource.loop = true;

                soundSources = new AudioSource[10];
                for (int i = 0; i < soundSources.Length; ++i)
                {
                    var source = gameObject.AddComponent<AudioSource>();
                    source.loop = false;
                    soundSources[i] = source;
                }

                if (defaultMusic != null)
                {
                    PlayMusic(defaultMusic, true, 1f);
                }
            }
        }

        public void PlayMusic(AudioClip clip, bool looping, float volumeScale)
        {
            if (musicSource.clip != clip && musicVolume > 0f && volumeScale > 0f)
            {
                musicSource.volume = musicVolume * volumeScale;
                musicSource.clip = clip;
                musicSource.loop = looping;
                musicSource.Play();
            }
        }

        public void playSound(AudioClip clip, float volumeScale, float pitch)
        {
            if (soundVolume > 0f && volumeScale > 0f)
            {
                AudioSource soundSource = soundSources[soundSourceIndex];
                soundSourceIndex = (soundSourceIndex + 1) % soundSources.Length;

                soundSource.clip = clip;
                soundSource.pitch = pitch;
                soundSource.volume = soundVolume * volumeScale;
                soundSource.PlayOneShot(clip);
            }
        }

        public void playSound(AudioClip clip, float volumeScale, float pitch, AudioSource source)
        {
            if (soundVolume > 0f && volumeScale > 0f)
            {
                source.clip = clip;
                source.pitch = pitch;
                source.volume = soundVolume * volumeScale;
                source.PlayOneShot(clip);
            }
        }

        public void PlayButtonClick()
        {
            clickSound.Play();
        }

        public void StopMusic()
        {
            musicSource.Stop();
        }

        public void PlayDefaultMusic()
        {
            if (musicVolume > 0)
            {
                musicSource.volume = musicVolume;
                musicSource.clip = defaultMusic;
                musicSource.loop = true;
                musicSource.Play();
            }
        }

        static public void PlaySound(AudioClip clip, float volumeScale, float pitch)
        {
            Instance.playSound(clip, volumeScale, pitch);
        }

        static public void PlaySound(AudioClip clip, float volumeScale, float pitch, AudioSource source)
        {
            Instance.playSound(clip, volumeScale, pitch, source);
        }

        /*
        static public void PlaySound(AudioClip clip, float volumeScale, float pitch, Vector3 position)
        {
            float dist = Vector3.Distance(position, Level.Instance.avatar.transform.position);
            if (dist < SoundCutoffDistance)
            {
                float distScale = 1.0f - (dist / SoundCutoffDistance);
                instance.playSound(clip, volumeScale * distScale, pitch);
            }
        }
        */

        static public float MusicVolume
        {
            get { return Instance != null ? Instance.musicVolume : 1f; }
            set
            {
                Instance.musicVolume = Mathf.Clamp01(value);
                var music = Instance.musicSource;
                music.volume = Instance.musicVolume;
                if (music.volume <= 0)
                {
                    music.Stop();
                }
                else
                {
                    if (!music.isPlaying)
                    {
                        music.Play();
                    }
                }
            }
        }

        static public float SoundVolume
        {
            get { return Instance != null ? Instance.soundVolume : 1f; }
            set
            {
                Instance.soundVolume = Mathf.Clamp01(value);
            }
        }
    }
}
